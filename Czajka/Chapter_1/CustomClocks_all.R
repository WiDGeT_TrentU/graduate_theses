#Custom Species Specific Clocks
#All species
#Figure 1

library(dplyr)
setwd("~/Dropbox/Methylation_ALLIANCE/Natalie_Czajka_MSc")

#####Black Bear
BearData=read.csv('EpigeneticClocks/BearOutput/FINAL/Bear_Clock_EpigeneticLogAge_PredictedValues.csv')
MAE_b <- abs(BearData$Age-BearData$DNAmAge)
median(MAE_b, na.rm=T)
tableB <- cbind(BearData$Age, BearData$DNAmAge, MAE_b)

jpeg("~/Dropbox/Methylation_ALLIANCE/Natalie_Czajka_MSc/EpigeneticClocks/BlackBearClock.jpeg")
coloursB <- c("hotpink", "royalblue")
plot(BearData$DNAmAge~BearData$Age, main = "Black bear (n=47)\nCor=0.95, MAE=1.33", ylab = "DNAm age prediction (log)", xlab = "Estimated age (years)", col=coloursB[factor(BearData$Female)], pch = c(17, 16)[factor(BearData$Female)], xlim=c(0, 20), ylim=c(0, 15))
abline(lm(BearData$DNAmAge~BearData$Age), lty=1, lwd=2, col="grey30")
abline(a=1, b=1, lty=2, col="grey30")
legend("topleft", legend = levels(factor(BearData$Female)), col=coloursB[factor(BearData$Female)], pch = c(17, 16)[factor(BearData$Female)])
dev.off()

####Mountain Goat 
MGData=read.csv('EpigeneticClocks/MountainGoatOutput/FINAL/MGoat_SepNormComBat_Clock_EpigeneticLogAge_PredictedValues.csv')
MAE_mg <- abs(MGData$Age-MGData$DNAmAge)
median(MAE_mg, na.rm=T)
tableMG <- cbind(MGData$Age, MGData$DNAmAge, MAE_mg)

jpeg("~/Dropbox/Methylation_ALLIANCE/Natalie_Czajka_MSc/EpigeneticClocks/MountainGoatClock.jpeg")
coloursMG <- c("hotpink", "royalblue")
plot(MGData$DNAmAge~MGData$Age, main = "Mountain goat (n=40)\nCor=0.97, MAE=0.61", ylab = "DNAm age prediction (log)", xlab = "Estimated age (years)", col=coloursMG[factor(MGData$Female)], pch = c(17, 16)[factor(MGData$Female)], xlim=c(0, 12), ylim=c(0, 12))
abline(lm(MGData$DNAmAge~MGData$Age), lty=1, lwd=2, col="grey30")
abline(a=1, b=1, lty=2, col="grey30")
legend("topleft", legend = levels(factor(MGData$Female)), col=c("hotpink", "royalblue"), pch = c(16, 17)[factor(MGData$Female)])
dev.off()

####Deer
DeerData=read.csv('EpigeneticClocks/DeerOutput/FINAL/Deer_Clock_EpigeneticLogAge_PredictedValues.csv') %>%
  dplyr::filter(!is.na(DNAmAge))
MAE_d <- abs(DeerData$Age-DeerData$DNAmAge)
median(MAE_d, na.rm=T)
tableD <- cbind(DeerData$Age, DeerData$DNAmAge, MAE_d)

jpeg("~/Dropbox/Methylation_ALLIANCE/Natalie_Czajka_MSc/EpigeneticClocks/WTDClock.jpeg")
coloursD <- c("hotpink", "royalblue")
plot(DeerData$DNAmAge~DeerData$Age, main = "White-tailed deer (n=33)\nCor=0.99, MAE=0.29", ylab = "DNAm age prediction (log)", xlab = "Estimated age (years)", col=coloursD[factor(DeerData$Female)], pch = c(17, 16)[factor(DeerData$Female)], xlim=c(0, 11), ylim=c(0, 11))
abline(lm(DeerData$DNAmAge~DeerData$Age), lty=1, lwd=2, col="grey30")
abline(a=1, b=1, lty=2, col="grey30")
legend("topleft", legend = levels(factor(DeerData$Female)), col=c("hotpink", "royalblue"), pch = c(17,16))
dev.off()

########################Combining Plots
par(mfrow = c(1, 3))
#
BearData=read.csv('EpigeneticClocks/BearOutput/FINAL/Bear_Clock_EpigeneticLogAge_PredictedValues.csv')
coloursB <- c("hotpink", "royalblue")
plot(BearData$DNAmAge~BearData$Age, main = "Black bear (n=47)\nCor=0.95, MAE=1.33", ylab = "DNAm age prediction (years)", xlab = "Estimated age (years)", col=coloursB[factor(BearData$Female)], pch = c(17, 16)[factor(BearData$Female)], xlim=c(0, 20), ylim=c(0, 20))
mtext("A", font=2, side=3, line=1, adj=-0.15, cex = 1)
abline(lm(BearData$DNAmAge~BearData$Age), lty=1, lwd=2, col="grey30")
abline(a=1, b=1, lty=2, col="grey30")
legend("topleft", legend = levels(factor(BearData$Female)), col=coloursB[factor(BearData$Female)], pch = c(17, 16)[factor(BearData$Female)])
#
DeerData=read.csv('EpigeneticClocks/DeerOutput/FINAL/Deer_Clock_EpigeneticLogAge_PredictedValues.csv') %>%
  dplyr::filter(!is.na(DNAmAge))
coloursD <- c("hotpink", "royalblue")
plot(DeerData$DNAmAge~DeerData$Age, main = "White-tailed deer (n=33)\nCor=0.99, MAE=0.29", ylab = "DNAm age prediction (years)", xlab = "Estimated age (years)", col=coloursD[factor(DeerData$Female)], pch = c(17, 16)[factor(DeerData$Female)], xlim=c(0, 12), ylim=c(0, 12))
mtext("B", font=2, side=3, line=1, adj=-0.15, cex = 1)
abline(lm(DeerData$DNAmAge~DeerData$Age), lty=1, lwd=2, col="grey30")
abline(a=1, b=1, lty=2, col="grey30")
legend("topleft", legend = levels(factor(DeerData$Female)), col=c("hotpink", "royalblue"), pch = c(17,16))
#
MGData=read.csv('EpigeneticClocks/MountainGoatOutput/FINAL/MGoat_SepNormComBat_Clock_EpigeneticLogAge_PredictedValues.csv')
coloursMG <- c("hotpink", "royalblue")
plot(MGData$DNAmAge~MGData$Age, main = "Mountain goat (n=40)\nCor=0.97, MAE=0.61", ylab = "DNAm age prediction (years)", xlab = "Estimated age (years)", col=coloursMG[factor(MGData$Female)], pch = c(17, 16)[factor(MGData$Female)], xlim=c(0, 12), ylim=c(0, 12))
mtext("C", font=2, side=3, line=1, adj=-0.15, cex = 1)
abline(lm(MGData$DNAmAge~MGData$Age), lty=1, lwd=2, col="grey30")
abline(a=1, b=1, lty=2, col="grey30")
legend("topleft", legend = levels(factor(MGData$Female)), col=c("hotpink", "royalblue"), pch = c(16, 17)[factor(MGData$Female)])
#
par(mfrow = c(1, 1)) #setting back to one per page
