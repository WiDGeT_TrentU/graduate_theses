# Proximate drivers of migration propensity

Code repository for the publication SProximate drivers of migration propensity: a meta-analysis across species, available on [BioRxiv]

## Table of contents

-   Abstract

-   Code repository description

## Abstract

Animal migration is multifaceted in nature, but the relative strength of different cues that trigger resulting patterns of migration is not well understood. Partially migratory populations offer an opportunity to test hypotheses about migration more broadly by comparing trait differences of migrants and residents. We quantitatively reviewed 45 studies that statistically modeled migration propensity, extracting132 effect sizes for internal and external proximate drivers across taxa. Our meta-analysis revealed that internal and external drivers had medium (Cohen’s d > 0.3) and large (Cohen’s d > 0.5) effect sizes on migration propensity respectively. Predator abundance and predation risk had a large effect, as did individual behaviour (e.g., personality). The abiotic environment and individual physiology had a medium effect on migration propensity. Of the studies that examined genetic divergence between migrants and residents, 64% found some genetic divergence between groups. These results clarify broad proximate drivers of migration and offer generalities across taxa.

## Code repository description

This repository was made for reproducibility purposes.

Description of the scripts in order of execution can be found in the .rmd and .html file named meta_analysis_effectsize_models