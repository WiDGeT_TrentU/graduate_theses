### Fet outlier detection for Poolseq ====
# Authors: Maegwin Bonar
# Purpose: Select outliers from fet values
# Inputs: igv file of the fet calculation generated using popoolation2, chromosome alignment table, chromosome labels
# Outputs: list of outliers
# License: /LICENSE.md 
# Title: 

### Packages ----
libs <- c('ggplot2','dplyr','data.table','plyr', 'ggpubr', 'sjmisc')
lapply(libs, require, character.only = TRUE)

### Input raw data ----
# Igv format file

fet <- read.table('input/pool_2500bp_80_fet.igv', header = T)
setDT(fet)

#### Separate file with all comparisons into single comparison groups
X12 <- fet[, c('Chromosome', 'End','X1.2')]

X13 <- fet[, c('Chromosome', 'End','X1.3')]

X14 <- fet[, c('Chromosome', 'End','X1.4')]

X15 <- fet[, c('Chromosome', 'End','X1.5')]

X23 <- fet[, c('Chromosome', 'End','X2.3')]

X24 <- fet[, c('Chromosome', 'End','X2.4')]

X25 <- fet[, c('Chromosome', 'End','X2.5')]

X34 <- fet[, c('Chromosome', 'End','X3.4')]

X35 <- fet[, c('Chromosome', 'End','X3.5')]

X45 <- fet[, c('Chromosome', 'End', 'X4.5')]

### Select the top 1% out of all relevant comparisions (in this case 6 comparisions of different migratory directions) ----
X12<- X12[X12$X1.2 > quantile(X12$X1.2,prob=1-1/100)]
X12[, 'ID' := paste(Chromosome, End, sep = '_')]

X14<- X14[X14$X1.4 > quantile(X14$X1.4,prob=1-1/100)]
X14[, 'ID' := paste(Chromosome, End, sep = '_')]

X15<- X15[X15$X1.5 > quantile(X15$X1.5,prob=1-1/100)]
X15[, 'ID' := paste(Chromosome, End, sep = '_')]


X23<- X23[X23$X2.3 > quantile(X23$X2.3,prob=1-1/100)]
X23[, 'ID' := paste(Chromosome, End, sep = '_')]


X34<- X34[X34$X3.4 > quantile(X34$X3.4,prob=1-1/100)]
X34[, 'ID' := paste(Chromosome, End, sep = '_')]


X35<- X35[X35$X3.5 > quantile(X35$X3.5,prob=1-1/100)]
X35[, 'ID' := paste(Chromosome, End, sep = '_')]


#merge groups together
top <- full_join(X12, X14, by = 'ID')
top <- full_join(top, X15, by = 'ID')
top <- full_join(top, X23, by = 'ID')
top <- full_join(top, X34, by = 'ID')
top <- full_join(top, X35, by = 'ID')

# keep only columns needed
top <- top[, c('ID', 'X1.2', 'X1.4', 'X1.5', 'X2.3', 'X3.4','X3.5')]
# count the number of NAs across the pools
top <- row_count(top, X1.2:X3.5, count = NA, append = TRUE)
# keep only those outliers that show up in 4/6 pools
top67 <- top[ rowcount <= 2, ]
top50 <- top[ rowcount <= 3, ]

### Do the same thing for comparisons of same migratory direction ----
X13<- X13[X13$X1.3 > quantile(X13$X1.3,prob=1-1/100)]
X13[, 'ID' := paste(Chromosome, End, sep = '_')]


X24<- X24[X24$X2.4 > quantile(X24$X2.4,prob=1-1/100)]
X24[, 'ID' := paste(Chromosome, End, sep = '_')]


X25<- X25[X25$X2.5 > quantile(X25$X2.5,prob=1-1/100)]
X25[, 'ID' := paste(Chromosome, End, sep = '_')]


X45<- X45[X45$X4.5 > quantile(X45$X4.5,prob=1-1/100)]
X45[, 'ID' := paste(Chromosome, End, sep = '_')]


#merge groups together
top_opp <- full_join(X13, X24, by = 'ID')
top_opp <- full_join(top_opp, X25, by = 'ID')
top_opp <- full_join(top_opp, X45, by = 'ID')
setDT(top_opp)

# keep only columns needed
top_opp <- top_opp[, c('ID', 'X1.3', 'X2.4', 'X2.5', 'X4.5')]

### join together top groups by ID to see if they show up in the opposite comparisons ----
# top SNPs that are in 4/6 comparisions
top_all67 <- left_join(top67, top_opp, by = 'ID')
top_all67<- row_count(top_all67, X1.3:X4.5, count = NA, append = TRUE)
# none of the comparisons of same migration direction
top_all67 <- top_all67[rowcount...13 == 4, ]

# top SNPs that are in 3/6 comparisions
top_all50 <- left_join(top50, top_opp, by = 'ID')
top_all50<- row_count(top_all50, X1.3:X4.5, count = NA, append = TRUE)
# none of the comparisons of same migration direction
top_all50 <- top_all50[rowcount...13 == 4, ]

# Keep only the ID column and split it back into scaffold and midpoint
top50 <- data.table(top_all50[, str_split(ID, "_", simplify = TRUE)])
# create Ending point of 2500bp window
top50$V3 <- as.numeric(top50$V2)-1250
# create end point of 2500bp window
top50$V4 <- as.numeric(top50$V2)+1250
# keep only relevant fields (chrom, start, end)
top50 <- top50[,c('V1', 'V3', 'V4')]

# Repeat for 4/6 window outlier snps
top67 <- data.table(top_all67[, str_split(ID, "_", simplify = TRUE)])
# End point
top67$V3 <- as.numeric(top67$V2)-1250
# end point
top67$V4 <- as.numeric(top67$V2)+1250
# keep only relevant fields (chrom, start, end)
top67 <- top67[,c('V1', 'V3', 'V4')]

# save
write.table(top67, 'output/fetOutlierWindows_top67.bed', row.names = FALSE, quote = FALSE, sep = "\t", col.names = FALSE)
write.table(top50, 'output/fetOutlierWindows_top50.bed', row.names = FALSE, quote = FALSE, sep = "\t", col.names = FALSE)
