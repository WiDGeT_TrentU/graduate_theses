### fet Plots for Poolseq ====
# Authors: Maegwin Bonar
# Purpose: generate manhattan plots
# Inputs: figv file of the fet calculation generated using popoolation2
# Outputs:list of outliers and manhattan plots
# License: /LICENSE.md 
# Title: 

### Packages ----
libs <- c('ggplot2','dplyr','data.table','plyr', 'ggpubr')
lapply(libs, require, character.only = TRUE)

### Input raw data ----
# Igv format file

fet <- read.table('input/pool_2500bp_80_fet.igv', header = T)

#Chromosome alignment
mummer <- read.table('input/wtd_cow_mummer_sorted_filtered.dms')
names(mummer) <- c("1", "2", "3", "4", "COW", "WTD")
mummer <- mummer[,c("COW","WTD")]

CHR_labels <- read.table("input/CHR_labels.txt")
names(CHR_labels) <- c("CHR","COW")

mummer_labels <- join(mummer, CHR_labels, by='COW')
mummer_labels$CHR[is.na(mummer_labels$CHR)] <- 32
mummer_labels<- mummer_labels[,c('WTD', 'CHR')]
names(mummer_labels) <- c("Chromosome", "CHR")

# join chromosome labels to fet file
fet_labels <- join(fet, mummer_labels, by='Chromosome')

# To make a manhattan plot in ggplot you need to get the cumulative position of 
# all the SNPs so that they all plot in order. I followed this tutorial to do 
# this but did it using data.table instead of dplyr since I don't know how to use that.
# https://www.r-graph-gallery.com/101_Manhattan_plot.html

#set initial dataset as datatable
setDT(fet_labels)

# this finds us the length of each chromosome
d <-fet_labels[, max(End), by = CHR]
# I ordered the datatable for some reason the chromosomes weren't in order
d <-d[order(CHR)]
# then you greate a total cumulative position of each chromosome
d[, tot := cumsum(V1)-V1]
# keep only the variables you need
d <- d[,c('CHR', 'tot')]

# Add the cumulative poisition to the initial dataset (I created a new one here)
fet_labels_gg <- as.data.table(join(fet_labels, d, by='CHR'))
# Add cumulative poisition of each SNP
fet_labels_gg[, BPcum := Start+tot, by = CHR]

# Here you are generating the center position of each chromosome so you can lable it properly
axisdf <- fet_labels_gg[, (max(BPcum) + min(BPcum) ) / 2, by = CHR]

# plot it

### NM vs NMS ----
NM_NMS <-ggplot(fet_labels_gg, aes(x=BPcum, y=X1.2)) +
  # Show all points
  geom_point( aes(color=as.factor(CHR)), alpha=0.8, size=1.3) +
  geom_hline(yintercept = -log10(0.001), color = "red", size=0.5, 
             alpha=0.5, linetype="dashed")+
  geom_hline(yintercept = -log10(0.005), color = "red", size=0.5, 
             alpha=0.5, linetype="dashed")+
  scale_color_manual(values = rep(c("grey", "skyblue"), 32)) +
  # custom X axis:
  scale_x_continuous(label = axisdf$CHR, breaks= axisdf$V1 ) +
  scale_y_continuous(expand = expansion(mult = c(0, 0.1)), n.breaks = 6) +     # remove space between plot area and x axis
  # Custom the theme:
  theme_bw() +
  theme( 
    legend.position="none",
    panel.border = element_blank(),
    panel.grid.major.x = element_blank(),
    panel.grid.minor.x = element_blank()
  )+
  xlab("Chromosome")+
  ylab("fet")+
  ggtitle('North Magnolia (North) vs. North Magnolia (South)')

png("output/Fet/NM_NMS_fet.png",width=1200,height=600,units="px",res=150, pointsize = 3)
print(NM_NMS)
dev.off()

### NM vs NR ----
NM_NR <- ggplot(fet_labels_gg, aes(x=BPcum, y=X1.3)) +
  # Show all points
  geom_point( aes(color=as.factor(CHR)), alpha=0.8, size=1.3) +
  scale_color_manual(values = rep(c("grey", "skyblue"), 32)) +
  geom_hline(yintercept = -log10(0.001), color = "red", size=0.5, 
             alpha=0.5, linetype="dashed")+
  geom_hline(yintercept = -log10(0.005), color = "red", size=0.5, 
             alpha=0.5, linetype="dashed")+
  # custom X axis:
  scale_x_continuous(label = axisdf$CHR, breaks= axisdf$V1 ) +
  scale_y_continuous(expand = expansion(mult = c(0, 0.1)), n.breaks = 6) +     # remove space between plot area and x axis
  # Custom the theme:
  theme_bw() +
  theme( 
    legend.position="none",
    panel.border = element_blank(),
    panel.grid.major.x = element_blank(),
    panel.grid.minor.x = element_blank()
  )+
  xlab("Chromosome")+
  ylab("fet")+
  ggtitle('North Magnolia (North) vs. North Ridge')

png("output/Fet/NM_NR_fet.png",width=1200,height=600,units="px",res=150, pointsize = 3)
print(NM_NR)
dev.off()

### NM vs RG ----
NM_RG <- ggplot(fet_labels_gg, aes(x=BPcum, y=X1.4)) +
  # Show all points
  geom_point( aes(color=as.factor(CHR)), alpha=0.8, size=1.3) +
  scale_color_manual(values = rep(c("grey", "skyblue"), 32)) +
  geom_hline(yintercept = -log10(0.001), color = "red", size=0.5, 
             alpha=0.5, linetype="dashed")+
  geom_hline(yintercept = -log10(0.005), color = "red", size=0.5, 
             alpha=0.5, linetype="dashed")+
  # custom X axis:
  scale_x_continuous(label = axisdf$CHR, breaks= axisdf$V1 ) +
  scale_y_continuous(expand = expansion(mult = c(0, 0.1)), n.breaks = 6) +     # remove space between plot area and x axis
  # Custom the theme:
  theme_bw() +
  theme( 
    legend.position="none",
    panel.border = element_blank(),
    panel.grid.major.x = element_blank(),
    panel.grid.minor.x = element_blank()
  )+
  xlab("Chromosome")+
  ylab("fet")+
  ggtitle('North Magnolia (North) vs. Ryan Gulch')

png("output/Fet/NM_RG_fet.png",width=1200,height=600,units="px",res=150, pointsize = 3)
print(NM_RG)
dev.off()

### NM vs SM ----
NM_SM <- ggplot(fet_labels_gg, aes(x=BPcum, y=X1.5)) +
  # Show all points
  geom_point( aes(color=as.factor(CHR)), alpha=0.8, size=1.3) +
  scale_color_manual(values = rep(c("grey", "skyblue"), 32)) +
  geom_hline(yintercept = -log10(0.001), color = "red", size=0.5, 
             alpha=0.5, linetype="dashed")+
  geom_hline(yintercept = -log10(0.005), color = "red", size=0.5, 
             alpha=0.5, linetype="dashed")+
  # custom X axis:
  scale_x_continuous(label = axisdf$CHR, breaks= axisdf$V1 ) +
  scale_y_continuous(expand = expansion(mult = c(0, 0.1)), n.breaks = 6) +     # remove space between plot area and x axis
  # Custom the theme:
  theme_bw() +
  theme( 
    legend.position="none",
    panel.border = element_blank(),
    panel.grid.major.x = element_blank(),
    panel.grid.minor.x = element_blank()
  )+
  xlab("Chromosome")+
  ylab("fet")+
  ggtitle('North Magnolia (North) vs. South Magnolia')

png("output/Fet/NM_SM_fet.png",width=1200,height=600,units="px",res=150, pointsize = 3)
print(NM_SM)
dev.off()

### NMS vs NR ----
NMS_NR <- ggplot(fet_labels_gg, aes(x=BPcum, y=X2.3)) +
  # Show all points
  geom_point( aes(color=as.factor(CHR)), alpha=0.8, size=1.3) +
  scale_color_manual(values = rep(c("grey", "skyblue"), 32)) +
  geom_hline(yintercept = -log10(0.001), color = "red", size=0.5, 
             alpha=0.5, linetype="dashed")+
  geom_hline(yintercept = -log10(0.005), color = "red", size=0.5, 
             alpha=0.5, linetype="dashed")+
  # custom X axis:
  scale_x_continuous(label = axisdf$CHR, breaks= axisdf$V1 ) +
  scale_y_continuous(expand = expansion(mult = c(0, 0.1)), n.breaks = 6) +     # remove space between plot area and x axis
  # Custom the theme:
  theme_bw() +
  theme( 
    legend.position="none",
    panel.border = element_blank(),
    panel.grid.major.x = element_blank(),
    panel.grid.minor.x = element_blank()
  )+
  xlab("Chromosome")+
  ylab("fet")+
  ggtitle('North Magnolia (South) vs. North Ridge')

png("output/Fet/NMS_NR_fet.png",width=1200,height=600,units="px",res=150, pointsize = 3)
print(NMS_NR)
dev.off()

### NMS vs RG ----
NMS_RG <- ggplot(fet_labels_gg, aes(x=BPcum, y=X2.4)) +
  # Show all points
  geom_point( aes(color=as.factor(CHR)), alpha=0.8, size=1.3) +
  scale_color_manual(values = rep(c("grey", "skyblue"), 32)) +
  geom_hline(yintercept = -log10(0.001), color = "red", size=0.5, 
             alpha=0.5, linetype="dashed")+
  geom_hline(yintercept = -log10(0.005), color = "red", size=0.5, 
             alpha=0.5, linetype="dashed")+
  # custom X axis:
  scale_x_continuous(label = axisdf$CHR, breaks= axisdf$V1 ) +
  scale_y_continuous(expand = expansion(mult = c(0, 0.1)), n.breaks = 6) +     # remove space between plot area and x axis
  # Custom the theme:
  theme_bw() +
  theme( 
    legend.position="none",
    panel.border = element_blank(),
    panel.grid.major.x = element_blank(),
    panel.grid.minor.x = element_blank()
  )+
  xlab("Chromosome")+
  ylab("fet")+
  ggtitle('North Magnolia (South) vs. Ryan Gulch')

png("output/Fet/NMS_RG_fet.png",width=1200,height=600,units="px",res=150, pointsize = 3)
print(NMS_RG)
dev.off()

### NMS vs SM ----
NMS_SM <- ggplot(fet_labels_gg, aes(x=BPcum, y=X2.5)) +
  # Show all points
  geom_point( aes(color=as.factor(CHR)), alpha=0.8, size=1.3) +
  scale_color_manual(values = rep(c("grey", "skyblue"), 32)) +
  geom_hline(yintercept = -log10(0.001), color = "red", size=0.5, 
             alpha=0.5, linetype="dashed")+
  geom_hline(yintercept = -log10(0.005), color = "red", size=0.5, 
             alpha=0.5, linetype="dashed")+
  # custom X axis:
  scale_x_continuous(label = axisdf$CHR, breaks= axisdf$V1 ) +
  scale_y_continuous(expand = expansion(mult = c(0, 0.1)), n.breaks = 6) +     # remove space between plot area and x axis
  # Custom the theme:
  theme_bw() +
  theme( 
    legend.position="none",
    panel.border = element_blank(),
    panel.grid.major.x = element_blank(),
    panel.grid.minor.x = element_blank()
  )+
  xlab("Chromosome")+
  ylab("fet")+
  ggtitle('North Magnolia (South) vs. South Magnolia')

png("output/Fet/NMS_SM_fet.png",width=1200,height=600,units="px",res=150, pointsize = 3)
print(NMS_SM)
dev.off()

### NR vs RG ----
NR_RG <- ggplot(fet_labels_gg, aes(x=BPcum, y=X3.4)) +
  # Show all points
  geom_point( aes(color=as.factor(CHR)), alpha=0.8, size=1.3) +
  scale_color_manual(values = rep(c("grey", "skyblue"), 32)) +
  geom_hline(yintercept = -log10(0.001), color = "red", size=0.5, 
             alpha=0.5, linetype="dashed")+
  geom_hline(yintercept = -log10(0.005), color = "red", size=0.5, 
             alpha=0.5, linetype="dashed")+
  # custom X axis:
  scale_x_continuous(label = axisdf$CHR, breaks= axisdf$V1 ) +
  scale_y_continuous(expand = expansion(mult = c(0, 0.1)), n.breaks = 6) +     # remove space between plot area and x axis
  # Custom the theme:
  theme_bw() +
  theme( 
    legend.position="none",
    panel.border = element_blank(),
    panel.grid.major.x = element_blank(),
    panel.grid.minor.x = element_blank()
  )+
  xlab("Chromosome")+
  ylab("fet")+
  ggtitle('North Ridge vs. Ryan Gulch')

png("output/Fet/NR_RG_fet.png",width=1200,height=600,units="px",res=150, pointsize = 3)
print(NR_RG)
dev.off()

### NR vs SM ----
NR_SM <- ggplot(fet_labels_gg, aes(x=BPcum, y=X3.5)) +
  # Show all points
  geom_point( aes(color=as.factor(CHR)), alpha=0.8, size=1.3) +
  scale_color_manual(values = rep(c("grey", "skyblue"), 32)) +
  geom_hline(yintercept = -log10(0.001), color = "red", size=0.5, 
             alpha=0.5, linetype="dashed")+
  geom_hline(yintercept = -log10(0.005), color = "red", size=0.5, 
             alpha=0.5, linetype="dashed")+
  # custom X axis:
  scale_x_continuous(label = axisdf$CHR, breaks= axisdf$V1 ) +
  scale_y_continuous(expand = expansion(mult = c(0, 0.1)), n.breaks = 6) +     # remove space between plot area and x axis
  # Custom the theme:
  theme_bw() +
  theme( 
    legend.position="none",
    panel.border = element_blank(),
    panel.grid.major.x = element_blank(),
    panel.grid.minor.x = element_blank()
  )+
  xlab("Chromosome")+
  ylab("fet")+
  ggtitle('North Ridge vs. South Magnolia')

png("output/Fet/NR_SM_fet.png",width=1200,height=600,units="px",res=150, pointsize = 3)
print(NR_SM)
dev.off()

### RG vs SM ----
RG_SM <- ggplot(fet_labels_gg, aes(x=BPcum, y=X4.5)) +
  # Show all points
  geom_point( aes(color=as.factor(CHR)), alpha=0.8, size=1.3) +
  scale_color_manual(values = rep(c("grey", "skyblue"), 32)) +
  geom_hline(yintercept = -log10(0.001), color = "red", size=0.5, 
             alpha=0.5, linetype="dashed")+
  geom_hline(yintercept = -log10(0.005), color = "red", size=0.5, 
             alpha=0.5, linetype="dashed")+
  # custom X axis:
  scale_x_continuous(label = axisdf$CHR, breaks= axisdf$V1 ) +
  scale_y_continuous(expand = expansion(mult = c(0, 0.1)), n.breaks = 6) +     # remove space between plot area and x axis
  # Custom the theme:
  theme_bw() +
  theme( 
    legend.position="none",
    panel.border = element_blank(),
    panel.grid.major.x = element_blank(),
    panel.grid.minor.x = element_blank()
  )+
  xlab("Chromosome")+
  ylab("fet")+
  ggtitle('Ryan Gulch vs. South Magnolia')

png("output/Fet/RG_SM_fet.png",width=1200,height=600,units="px",res=150, pointsize = 3)
print(RG_SM)
dev.off()

### put together in plots ----
png("output/Fet/Comparisonsfet.png",width=2400,height=1200,units="px",res=150, pointsize = 3)
ggarrange(NM_NMS, NM_RG, NM_SM, NMS_NR, NR_RG, NR_SM,
          ncol = 2, nrow = 3)
dev.off()

png("output/Fet/Fet/Comparisonsfet_opp.png",width=2400,height=1200,units="px",res=150, pointsize = 3)
ggarrange(NM_NR, NMS_RG, NMS_SM, RG_SM,
          ncol = 2, nrow = 2)
dev.off()
