#### Resequencing_SOP ####
#all 14 steps found at https://gitlab.com/WiDGeT_TrentU/Genome_analysis/-/tree/master/Resequencing_SOP####

#to test jobs before submitting them:
salloc --cpus-per-task=2 --mem=8G --account=def-shaferab --time=2:0:0

## 02_trimmomaticPE.sh ##

#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=trimmomatic
#SBATCH --cpus-per-task=16
#SBATCH --mem=16G
#SBATCH --time=0-06:00 # time (DD-HH:MM)

module load trimmomatic 
java -jar $EBROOTTRIMMOMATIC/trimmomatic-0.36.jar PE -threads 16 -phred33 ${1}_outR1P.fastq ${1}_outR2P.fastq ${1}_trim__outR1P.fastq ${1}_NAtrim__outR1P.fastq ${1}_trim__outR2P.fastq ${1}_NAtrim__outR2P.fastq ILLUMINACLIP:TruSeq3-PE.fa:2:30:10 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:36

# make a script to run the timmomatic script above
#02_trimmomatic_script_RUN.sh

mkdir trimmed_slurm
for f in $(ls *.fastq | cut -f1 -d'_' | uniq)
do
echo ${f}
sbatch -o /home/modedato/working/Caribou/gaspesie/${f}-%A.out trimmomaticPE.sh ${f}
sleep 10
done

#then must indec the genome for all next steps
## 03_index.bash ##

#!/bin/bash
#SBATCH --time=1-00:00:00
#SBATCH --account=rrg-shaferab
#SBATCH --mem=8G
module load picard
module load bwa
module load gatk/3.8
module load samtools

java -Xmx1G -jar $EBROOTPICARD/picard.jar CreateSequenceDictionary R=caribou_genome.fa O=caribou_genome.dict
samtools faidx caribou_genome.fa
bwa index caribou_genome.fa -a bwtsw

## 04_bwa-sortSE.sh ##

#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=bwa-bam-sort
#SBATCH --cpus-per-task=30
#SBATCH --mem=48G
#SBATCH --time=0-03:00 # time (DD-HH:MM)
#SBATCH --output=%x-%j.out

module load bwa
module load samtools
module load sambamba

echo ${1}
bwa mem -M -t 30 \
-R "@RG\\tID:${1}\\tSM:${1}\\tLB:${1}\\tPL:ILLUMINA" \
/home/modedato/working/Caribou/raw_data/caribou_genome.fa \
${1}_trim__outR1P.fastq ${1}_trim__outR2P.fastq | \
samtools sort -@ 30 \
-o /home/modedato/working/Caribou/raw_data/${1}.sorted_reads.bam - &&
/home/modedato/bin/sambamba-0.7.0-linux-static flagstat \
--nthreads=30 \
 /home/modedato/working/Caribou/raw_data/${1}.sorted_reads.bam \
> /home/modedato/working/Caribou/raw_data/${1}.sorted_reads.flagstat
#END

#make script to run 04_bwa-sort_script_RUN.sh 

## 05_picard-dup.sh ##
#Picard MarkDuplicates

#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=remove-dups
#SBATCH --cpus-per-task=30
#SBATCH --mem=125G
#SBATCH --time=0-12:00 # time (DD-HH:MM)

module load picard
module load samtools
module load sambamba
echo ${1}
java -Xmx120G -jar $EBROOTPICARD/picard.jar MarkDuplicates \
INPUT=/home/modedato/working/Caribou/mapped/${1}.sorted_reads.bam \
OUTPUT=/home/modedato/working/Caribou/dups/${1}.deduped_reads.bam \
USE_JDK_DEFLATER=true USE_JDK_INFLATER=true \
ASSUME_SORT_ORDER=coordinate \
REMOVE_DUPLICATES=true REMOVE_SEQUENCING_DUPLICATES=true \
METRICS_FILE=/home/modedato/working/Caribou/dups/metrics/${1}.deduped.picard &&
/home/modedato/bin/sambamba-0.7.0-linux-static flagstat \
--nthreads=30 \
/home/modedato/working/Caribou/dups/${1}.deduped_reads.bam \
> /home/modedato/working/Caribou/dups/flagstat/${1}.deduped_reads.flagstat
#END

#script to run 
## 05_picard-dup_script_RUN.sh ##
cd /home/modedato/working/Caribou
mkdir dups; mkdir dups/dups_slurm; mkdir dups/flagstat; mkdir dups/metrics; cd mapped
for f in $(ls *.bam | sed 's/.sorted_reads.bam//' | sort -u)
do
echo ${f}
sbatch -o /home/modedato/working/Caribou/dups/dups_slurm/${f}-%A.out picard-dup.sh ${f}
sleep 10
done

## 06_unique.sh ##
#select only unique mapped reads

#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=unique
#SBATCH --cpus-per-task=30
#SBATCH --mem=12G
#SBATCH --time=0-03:00 # time (DD-HH:MM)

module load samtools
module load sambamba
echo ${1}
/home/modedato/bin/sambamba-0.7.0-linux-static view \
--nthreads=30 --with-header --format=bam --show-progress \
-F "mapping_quality >= 1 and not (unmapped or secondary_alignment) and not ([XA] != null or [SA] != null)" \
/home/modedato/working/Caribou/dups/${1}.deduped_reads.bam \
-o /home/modedato/working/Caribou/unique/${1}.unique_reads.bam &&
/home/modedato/bin/sambamba-0.7.0-linux-static flagstat \
--nthreads=30 \
/home/modedato/working/Caribou/unique/${1}.unique_reads.bam \
> /home/modedato/working/Caribou/unique/flagstat/${1}.unique_reads.flagstat

#make script to run

## 06_unique_RUN.sh ##
cd /home/modedato/working/Caribou
mkdir unique; mkdir unique/unique_slurm; mkdir unique/flagstat; cd dups
for f in $(ls *.bam | sed 's/.deduped_reads.bam//' | sort -u)
do
echo ${f}
sbatch -o /home/modedato/working/Caribou/unique/unique_slurm/${f}-%A.out unique.sh ${f}
sleep 10
done

## 07a_realign.sh ##
nano realign.sh 

#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=unique
#SBATCH --cpus-per-task=30
#SBATCH --mem=32G
#SBATCH --time=0-12:00 # time (DD-HH:MM)
module load gatk/3.8
module load sambamba
echo ${1}
java -Xmx30G -jar $EBROOTGATK/GenomeAnalysisTK.jar \
-T RealignerTargetCreator \
--use_jdk_deflater --use_jdk_inflater \
-nt 30 \
-R /home/modedato/working/Caribou/index_genome/caribou_genome.fa \
-I /home/modedato/working/Caribou/unique/${1}.unique_reads.bam \
-o /home/modedato/working/Caribou/clean/intervals/${1}.realign.intervals &&
java -Xmx30G -jar $EBROOTGATK/GenomeAnalysisTK.jar \
-T IndelRealigner \
--use_jdk_deflater --use_jdk_inflater \
-R /home/modedato/working/Caribou/index_genome/caribou_genome.fa \ \
-I /home/modedato/working/Caribou/unique/${1}.unique_reads.bam \
-targetIntervals /home/modedato/working/Caribou/clean/intervals/${1}.realign.intervals \
-o /home/modedato/working/Caribou/clean/flagstat/${1}.realigned.bam &&
/home/modedato/bin/sambamba-0.7.0-linux-static flagstat \
> /home/modedato/working/Caribou/clean/flagstat/${1}.realigned.flagstat


###### 07b_realign_script_RUN.sh ######

nano realign_script_RUN.sh 
#Perform indel realignment
cd /home/modedato/working/Caribou/
mkdir clean; mkdir clean/clean_slurm; mkdir clean/flagstat; mkdir clean/intervals; cd unique
for f in $(ls *.bam | sed 's/.unique_reads.bam//' | sort -u)
do
echo ${f}
sbatch -o /home/modedato/working/Caribou/clean/clean_slurm/${f}-%A.out realign.sh ${f}
sleep 10
done

###### 08a_mpileup.bash ######
#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=mpileup_par
#SBATCH --ntasks=32 # number of MPI process per nodeG
#SBATCH --mem-per-cpu=16G
#SBATCH --time=12:00:00

module load bcftools
module load samtools
mkdir mpileup_run

#going to split up the genome into scaffolds to help with run - do before submitting job
#awk 'BEGIN {FS="\t"}; {print $1 FS "0" FS $2}' caribou_genome.fa.fai > ./mpileup_coord.bed
#awk 'BEGIN {FS="\t"}; {print $1 FS "1" FS $2}' caribou_genome.fa.fai > ./mpileup_coord.list


#make a list of bamfiles from working directory before running script
#ls -d /home/modedato/working/Caribou/clean/realigned/*bam > mpileup_list - do before submitting job

#needs full paths 
cat /home/modedato/working/Caribou/mpileup_coord.list | \
awk 'FS="\t", OFS="" {print $1,":"$2"-"$3}' | \
parallel -j 32 \
"bcftools mpileup --output-type u \
--fasta-ref /home/modedato/working/Caribou/index_genome/caribou_genome.fa \
-r {} -q 20 -Q 20 \
--bam-list /home/modedato/working/Caribou/mpileup_list | \
bcftools call \
-m -v -Ov -o /home/modedato/working/Caribou/mpileup_run/{}.vcf && \
echo {} is complete"
#single comand for reference
#bcftools mpileup -Ou -f caribou_genome.fa -b mpileup_list | bcftools call -mv -Ob -o /mpileup_run/mpileup_raw.bcf --threads 10

#only ended up used ~4GB so could ask for a lot less

###### 08b_concat_mpileup.bash ######

#bcftools_concat.sh
#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=bcftools_concat
#SBATCH --cpus-per-task=1
#SBATCH --mem=48G
#SBATCH --time=00-03:00

module load bcftools
module load samtools

ls -d mpileup_run/* > mpileup_file.list

bcftools concat \
--file-list /home/modedato/working/Caribou/mpileup_run/mpileup_file.list \
--output /home/modedato/working/Caribou/mpileup_run/mpileup_raw.vcf \
--output-type v && \
echo your job is complete
#END

#### 9a_gatk_unified_par.sh ####

#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=gatk_par
#SBATCH --cpus-per-task=32 # number of MPI process per node
#SBATCH --mem=450G
#SBATCH --time=00-24:00

mkdir gatk_run

module load gatk/3.8
cat mpileup_coord.list | \
awk 'FS="\t", OFS="" {print $1,":"$2"-"$3}' | \
parallel -j 32 \
"java -Xmx512G -jar $EBROOTGATK/GenomeAnalysisTK.jar -T UnifiedGenotyper -L {} -I bam.list -R /home/modedato/working/Caribou/index_genome/caribou_genome.fa -mbq 20 -glm BOTH -o gatk_run/{}.vcf && \
echo {} is complete"

#slurmstepd: error: Detected 194 oom-kill event(s) in step 31645692.batch cgroup.
#skipped using this, used freebayes and angsd instead 


###### 10a_freebayes_par.bash ######
nano freebayes_par.sh

#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=freebayes_par
#SBATCH --cpus-per-task=32 # number of MPI process per node
#SBATCH --mem=512G
#SBATCH --time=00-18:00

#might choose to request more memory but only used 2% of 500GB on 13 6x deer

#git clone --recursive git://github.com/ekg/freebayes.git
#cd freebayes
#make
mkdir freebayes_run


cat mpileup_coord.list | \
awk 'FS="\t", OFS="" {print $1,":"$2"-"$3}' | \
parallel -j 32 \
"freebayes/bin/freebayes -L bam.list -f /home/modedato/working/Caribou/index_genome/caribou_genome.fa -r {} -C 2 -3 40 -P 0.0001 -q 20 -W 1,3 -S 4 -M 3 -B 25 -E 3 -v freebayes_run/{}.vcf && \
echo {} is complete"


###### 10b_concat_freebayes.bash ######

#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=bcftools_concat
#SBATCH --cpus-per-task=1
#SBATCH --mem=48G
#SBATCH --time=00-03:00

module load bcftools
module load samtools

ls -d freebayes_run/*.vcf > freebayes_file.list
bcftools concat \
--file-list /home/modedato/working/Caribou/freebayes_file.list \
--output /home/modedato/working/Caribou/freebayes_raw.vcf \
--output-type v && \
echo your job is complete
#END

###### 11_angsd.bash ######

#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=angsd
#SBATCH --mem 150G
#SBATCH --cpus-per-task 10
#SBATCH --time=04-00:00
module load angsd
angsd -bam mpileup_list -doMajorMinor 1 -domaf 1 -out caribou.angsd -doVcf 1 -nThreads 10 -doGeno -4 -doPost 1 -gl 2 -SNP_pval 1e-6 -minMapQ 20 -minQ 20 -doCounts 1 -skipTriallelic 1 -doGlf 2
#note the VCF output is 4.2 and can't be read by vcftools as is; continuing through until phasing will produce a readable VCF
#took 3 days and 57G

#to get the output for plink, this needs the reference genome 
angsd -bam bam.list -out plinkinput -doPlink 2 -doGeno -4 -doPost 1 -doMajorMinor 1 -GL 1 -doCounts 1 -doMaf 2 -postCutoff 0.95 -SNP_pval 1e-6 -uniqueonly 1 -remove_bads 1 -C 50 -baq 1 -ref /home/modedato/working/Caribou/index_genome/caribou_genome.fa

###editing angsd 
#Editing angsd vcf to be compatable with further steps and vcftools
#make file that includes new header called vcf_head
#in this file will be: ##fileformat=VCFv4.2
#Delete the first row containing ##fileformat=VCFv4.2(angsd version)
sed 1d ANGSD_OUTPUT.vcf > FILE.vcf
#insert new first line with: ##fileformat=VCFv4.2
cat vcf_head FILE.vcf > FILE2.vcf

#essentially the first line of the vcf file changes from:
##fileformat=VCFv4.2(angsd version)
#to:
##fileformat=VCFv4.2


###### 12_VCF_filter_by_scaffold_length.bash ######
nano VCF_filter_by_scaffold_length.sh

#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=filter_vcs
#SBATCH --mem=16G
#SBATCH --ntasks=1
#SBATCH --time=00-03:00
#SBATCH --output=%x-%j.out


module load samtools
module load vcftools
module load bcftools
GENOME="caribou_genome"
VCF="caribou.angsd"

#index with samtools if not already
#samtools faidx $GENOME.fa

#prints index and uses awk command to report scaffolds >100000 bp (or 100kb)
cut -f1,2 $GENOME.fa.fai | sort -k 2 -V | awk '{if($2>100000)print$1}' > scaffold_filter
cat scaffold_filter | sed -n -e 'H;${x;s/\n/,/g;s/^,//;p;}'>scaffold_filter2
file="scaffold_filter2"
scaff=$(cat "$file")        #the output of 'cat $file' is assigned to the $name variable

bgzip $VCF.vcf
tabix $VCF.vcf.gz

bcftools filter -r $scaff $VCF.vcf.gz > $VCF.2.vcf

#count SNPs per scaffold as we want to drop scaffolds with only 1 SNP
grep -v "^#" $VCF.2.vcf | cut -f 1 | sort | uniq -c | sort -g > snp_scaf_count


###### 14_phase_VCF.bash ######

#Enter "java -jar beagle.27Jan18.7e1.jar" for a summary of command line arguments
#statistical estimation of haplotypes in a genome (groups of genes that were inherited together)
#window=[positive number] the number of markers per window
#Finally with an ask of 7 days and overlap=3000 window=100000

nano phase_VCF.sh

#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=beagle-phase
#SBATCH --mem 8G
#SBATCH --time=07-00:00
#SBATCH --output=%x-%j.out

#angsd vcf
#wget https://faculty.washington.edu/browning/beagle/beagle.27Jan18.7e1.jar
java -Xmx16g -Djava.awt.headless=true -jar beagle.27Jan18.7e1.jar gtgl=angsd_fixed.vcf impute=false overlap=3000 window=100000 gprobs=TRUE out=angsd.phased

#Total run time:                164 hours 40 minutes 54 seconds
#945 windows 
#Number of markers:             28847672

#angsd files is called angsd.phased.vcf 

# B L Browning, Y Zhou, and S R Browning (2018). A one-penny imputed genome from next generation reference panels. Am J Hum Genet 103(3):338-348. doi:10.1016/j.ajhg.2018.07.015






