#Script to estimate dN/dS from single-copy genes from OrthoFinder output using aBSREL
#Main author: Marie-Laurence Cossette
#code is based on resources (listed below)

##########
#Resources
##########

#aBSREL-HYPHY: http://hyphy.org/methods/selection-methods/
#tree prep for aBSREL: http://phylotree.hyphy.org/
#aBSREL outputs visualize: http://vision.hyphy.org/absrel

#I think in this script I didn't keep track of all the moving between directory so might run into errors if you don't keep track of how you organize yours
#so keep this in mind when running script, might want to organize them in a more efficient way
#this is probaby my least efficient script - a lot of it is fixing files to make them good to use for the analysis

### 1. Prepping files and requirements ###

#we will be using pal2nal to convert the multiple sequence alignment for each of the 533 single-copy genes identified by OrthoFinder into a codon alignments

#I already have miniconda installed so will use that the get pal2nal to work
#create new environment and set it up for pal2nal
conda activate
conda create -n pal
conda activate pal
conda config --add channels conda-forge
conda config --add channels defaults
conda config --add channels bioconda
conda config --add channels r
conda install perl
conda install perl-getopt-long
conda install pal2nal

#make directory for analysis files
mkdir DnDs
cd DnDs
mkdir CDS
cd CDS

#need to get all of the CDS sequences from NCBI for each species except maritime shrew
rsync -av rsync://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/296/755/GCF_000296755.1_EriEur2.0/GCF_000296755.1_EriEur2.0_cds_from_genomic.fna.gz ./
#1ae347dce71e853752e7195534a8f2d1
rsync -av rsync://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/002/285/GCF_000002285.5_Dog10K_Boxer_Tasha/GCF_000002285.5_Dog10K_Boxer_Tasha_cds_from_genomic.fna.gz  ./
#3c07e6c78da6577ba724e0b1a234a6d7 
rsync -av rsync://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/001/405/GCF_000001405.40_GRCh38.p14/GCF_000001405.40_GRCh38.p14_cds_from_genomic.fna.gz ./
#43142f505e619a2c365b641c88ed7d5e
rsync -av rsync://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/001/635/GCF_000001635.27_GRCm39/GCF_000001635.27_GRCm39_cds_from_genomic.fna.gz ./
#95f1452b8bfaa39d1518121b039ed3ce
rsync -av rsync://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/027/595/985/GCF_027595985.1_mSorAra2.pri/GCF_027595985.1_mSorAra2.pri_cds_from_genomic.fna.gz  ./
#230972b2e05c264e06060ab96e8f7468
rsync -av rsync://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/024/139/225/GCF_024139225.1_mSunEtr1.pri.cur/GCF_024139225.1_mSunEtr1.pri.cur_cds_from_genomic.fna.gz ./
#d663806b70ef7efdb03d90fb26229f60
rsync -av rsync://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/308/155/GCF_000308155.1_EptFus1.0/GCF_000308155.1_EptFus1.0_cds_from_genomic.fna.gz ./
#97dd960c1addaf79bb36e095e9348f6b
rsync -av rsync://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/260/355/GCF_000260355.1_ConCri1.0/GCF_000260355.1_ConCri1.0_cds_from_genomic.fna.gz  ./
#db24c349e44857a4f1f88c89578e4b1e
rsync -av rsync://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/331/955/GCF_000331955.2_Oorc_1.1/GCF_000331955.2_Oorc_1.1_cds_from_genomic.fna.gz   ./
#e22c06182fa4d8a5d0e14ae5332b58fb
rsync -av rsync://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/001/905/GCF_000001905.1_Loxafr3.0/GCF_000001905.1_Loxafr3.0_cds_from_genomic.fna.gz  ./
#eed56545b5e428014c7ec68b4fb72563 
rsync -av rsync://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/151/735/GCF_000151735.1_Cavpor3.0/GCF_000151735.1_Cavpor3.0_cds_from_genomic.fna.gz ./
#0a5855226a34f024d5ee0e28464bcc89
rsync -av rsync://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/002/863/925/GCF_002863925.1_EquCab3.0/GCF_002863925.1_EquCab3.0_cds_from_genomic.fna.gz ./
#785c14c148710a33b24e3d566468c5de
rsync -av rsync://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/015/227/675/GCF_015227675.2_mRatBN7.2/GCF_015227675.2_mRatBN7.2_cds_from_genomic.fna.gz  ./
#cf62f12cd7cdc7465d63bdec545e204f
rsync -av rsync://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/292/845/GCF_000292845.1_OchPri3.0/GCF_000292845.1_OchPri3.0_cds_from_genomic.fna.gz  ./
#64c162fb75999b1cd149e2ef5d5fa4e7
rsync -av rsync://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/165/445/GCF_000165445.2_Mmur_3.0/GCF_000165445.2_Mmur_3.0_cds_from_genomic.fna.gz   ./
#ab49b68e7e31ef8379b915122341206c
rsync -av rsync://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/325/575/GCF_000325575.1_ASM32557v1/GCF_000325575.1_ASM32557v1_cds_from_genomic.fna.gz  ./
#0e99470ab085d5a3ca27409503d05908
rsync -av rsync://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/317/375/GCF_000317375.1_MicOch1.0/GCF_000317375.1_MicOch1.0_cds_from_genomic.fna.gz   ./
#1277a2074b23f78da50e1486a224298b
rsync -av rsync://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/003/025/GCF_000003025.6_Sscrofa11.1/GCF_000003025.6_Sscrofa11.1_cds_from_genomic.fna.gz ./
#e2359d8be6288f77cad1f10d83c46a60
rsync -av rsync://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/029/834/395/GCF_029834395.1_SorFum_2.1/GCF_029834395.1_SorFum_2.1_cds_from_genomic.fna.gz   ./
#523d346203b6fa1d3afda8390496435d

#unzip and change names for each file to shorten them
gunzip GCF_000181275.1_SorAra2.0_cds_from_genomic.fna.gz
mv GCF_000181275.1_SorAra2.0_cds_from_genomic.fna SorAra_CDS.fna

#download maritime shrew from my computer
scp Downloads/Sorex-sorex_maritimensis-v1.0.a1.6462a91a6bfa2-publish.CDS.fna  mlcosset@graham.sharcnet.ca:///home/mlcosset/scratch/genes/DnDs

#in CDS directory for each species fna clean up headers to only have protein ID left
#this takes care of proteins starting with XP IDs
for i in *.fna; do sed '/^>/s/.*[ \t]*\(XP_[0-9^.]*\).*/>\1/' $i > ${i}.1; done 
#do same thing for protein ID starting with NP_ instead of XP_
for i in *.1; do sed '/^>/s/.*[ \t]*\(NP_[0-9^.]*\).*/>\1/' $i > ${i}.2; done 

#need to also clean up 
##need to double check the files, there might be a step missing here but it's not in my notes
sed '/^>/ s/ .*//' SorAra_CDS.fna > SorMar_CDS.fna.1
sed '/^>/s/.\{4\}$//' SorMar_CDS.fna.1 > SorMar_CDS.fna.1.2
SorMar_CDS.fna.1.2 > SorMar_CDS.fna_manual

#move all the cleaned up fna files in another directory
rm *1
mkdir fna
mv *fna fna
mv SorMar_CDS.fna_manual fna/

#from the OrthoFinder results go to the multiple alignement directory and select the 533 fa. that match the single-copy directory and bring them over to the DnDs directory
#first thing is figure out how to only move the 533 multiple aligments that we want
#so copy the single-copy ortholog directory
cp -r /home/mlcosset/scratch/genes/OrthoFinder-2.5.4/FINAL/output_june30/Results_Jun30/Single_Copy_Orthologue_Sequences/ /home/mlcosset/scratch/genes/DnDs/
#make list of file names in the directory
cd Single_Copy_Orthologue_Sequences
ls > list.txt

#move list from single-copy directory to multiple sequence alignment directory
mv list.txt /home/mlcosset/scratch/genes/OrthoFinder-2.5.4/FINAL/output_june30/Results_Jun30/MultipleSequenceAlignments/

#go to single copy directory in DnDs folder
cd /home/mlcosset/scratch/genes/DnDs/Single_Copy_Orthologue_Sequences

#make file with list of headers in each file
for i in *.fa; do grep ">" $i > ${i}.list.txt; done 
#remove .polypeptide for sormar bit in these lists
for i in *.txt; do sed -i 's/.polypeptide//' $i; done
#need to remove > from each line *in list files like above
for i in *txt; do sed -i 's/^.//'  $i; done

cd ../
#make new directory that will have the list of all the 533 single-copy genes that we want
mkdir list
#move all files we made above to the new directory
mv Single_Copy_Orthologue_Sequences/*txt list/

#now need to pull each CDS sequence that matches the headers in each orthologue list and make files for each

#merge all of the CDS files toegether because if I try a double loop to extract 533 wanted genes doesnt get all the sequences
#this work because each animal and protein have unique id
cd CDS
cat *fna.1.2 > ALL_CDS.fa

#loop from each list to take id and make an fq with those sequnces
#takes a while
cd ..
mkdir fq
module load seqtk
for i in list/*txt; do seqtk subseq CDS/ALL_CDS.fa $i > ${i}.fq; done

#move all these fq to new folder
mkdir fq
mv list/*fq fq/

#want to check if everything seems to be there
cd fq
grep ">" * | wc -l
#should have 533*20=10660
wc -l *

#because human has doubles in CDS, some fq had more than necessary, basically doubles or triples of some proteins so since I dont have that many to work with I manually cleaned it out with nano. 
#if some of the replicates were different sizes I kept the longest
#must be a better way if you have more files but this is what I did

conda activate pham
#installed pal2nal on this env

#make sure you are out of the DnDs directory
ls DnDs > list.txt
#remove last 3 carachters 
sed  's/.\{3\}$//' list.txt > list_2.txt
#Remove first row that says list list from top with nano, it’ll be there

#cat and copy the list to do this
#In parantherse put that copied cat output
cat list_2.txt
#should look like this, but with all your file names
list="OG0012840
OG0012841
OG0012843
OG0012844
OG0012845
OG0012847
OG0012850
OG0012861 
..."

#run pal2nal, it will run through all the files in that list
for i in $list; do 
pal2nal.pl alignments/$i.fa fq/${i}.fa.list.txt.fq -output fasta > ${i}.fasta;
done;

#make directory for outputs and move all output fastas from pal2nal there
mkdir done
mv *fasta done

#need to clean up names in headers for each species to match tree
#(LoxAfr,((((CanFam,EquCab),((PteAle,EptFus),(OrcOrc,SusScr))),(ConCri,(EriEur,((SorCin,(SorMar,SorAra)),SunEtr)))),((MicMur,Hg38),((CavPor,(MicOch,(Mm39,RatNor))),OchPri))))
#remove rest of the name that’s not the species abbreviation like above
#make new directory for cleaned up fasta
mkdir clean_fa
#go in directory with all the fastas from pal2nal clean up headers and move them to new clean directory
cd done
for i in *.fasta; do sed "s/[_].*//" $i > ${i}.clean.fasta; done
cd ..
mv done/*clean.fasta clean_fa/

### 2. Runnning aBSREL ###

#use aBSREL to find lineages which have experienced episodic diversifying selection
#aBSREL will test, for each branch (or branch of interest) in the phylogeny, whether a proportion of sites have evolved under positive selection.
#we will run aBSREL twice
#1.Perform an exploratory analysis where all branches are tested for positive selection. In this scenario, p-values at each branch must be corrected for multiple testing (using the Holm-Bonferroni correction). Due to multiple testing, the exploratory approach has much lower power compared to the other approach.
#2.Test a specific hypothesis by a priori selecting a set of "foreground" branches to test for positive selection (shrew nodes and branches as foreground).

#http://phylotree.hyphy.org
#use this website to make tree for aBSREL
#for the normal run with no foreground use basic tree and name that file "tree.nwk"
#(LoxAfr,((((CanFam,EquCab),((PteAle,EptFus),(OrcOrc,SusScr))),(ConCri,(EriEur,((SorCin,(SorMar,SorAra)),SunEtr)))),((MicMur,Hg38),((CavPor,(MicOch,(Mm39,RatNor))),OchPri))));
#this is the tre with shrews as foreground, name this file "shrew_ancestor_tree.nwk"
#(LoxAfr,((((CanFam,EquCab),((PteAle,EptFus),(OrcOrc,SusScr))),(ConCri,(EriEur,((SorCin{Foreground,Foreground},(SorMar{Foreground,Foreground},SorAra{Foreground,Foreground}){Foreground,Foreground}){Foreground,Foreground},SunEtr{Foreground,Foreground}){Foreground,Foreground}))),((MicMur,Hg38),((CavPor,(MicOch,(Mm39,RatNor))),OchPri))));

#no foreground run
#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=aBSREL.bash
#SBATCH --mem=0
#SBATCH --cpus-per-task=32
#SBATCH --output=%x_%j
#SBATCH --time=00-23:59# time (DD-HH:MM)
#SBATCH --mail-type=ALL
#SBATCH --mail-user=mcossette@trentu.ca
conda init bash
conda activate pham
for i in clean_fa/*fasta; do hyphy absrel --alignment "$i" --tree "tree.nwk" --output "${i}.json"; done

#run with shrew ancestor as foreground branch
#you can also split files in different directories to run faster, I did it in different batches because takes time
#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=aBSREL_OG0014.bash
#SBATCH --mem=0
#SBATCH --cpus-per-task=32
#SBATCH --output=%x_%j
#SBATCH --time=00-23:59# time (DD-HH:MM)
#SBATCH --mail-type=ALL
#SBATCH --mail-user=mcossette@trentu.ca
conda init bash
conda activate pham
for i in clean_fa/*fasta; do hyphy absrel --alignment "$i" --tree "shrew_ancestor_tree.nwk" --branches "Foreground,Foreground" --output "${i}.shrew_ancestor_foreground.json"; done

#can use this website to visualize outputs
#http://vision.hyphy.org/absrel

###end!
