#Script to identify orthogroups using OrthoFinder
#Main author: Marie-Laurence Cossette
#code is based on resources (listed below) 

##########
#Resources
##########

#Orthofinder - https://davidemms.github.io/menu/tutorials.html

#https://github.com/davidemms/OrthoFinder/releases/tag/2.5.4
#download source code (tar.gz) of version 2.5.4 from link above
#then send that downloaded file fom my computer to graham cluster
scp Downloads/OrthoFinder-2.5.4.tar mlcosset@graham.sharcnet.ca:///home/mlcosset/scratch/genes
tar -xvf OrthoFinder-2.5.4.tar
cd OrthoFinder-2.5.4
pip install scipy 

#make a directory for the proteome files
mkdir proteomes
cd proteomes 

#download the NCBI protein.faa from RefSeq to keep the analysis consistent with the genome versions used for the accelerated region analysis
#to make sure that they are the same genome versions as ARs, can use following links
#http://hgdownload.soe.ucsc.edu/goldenPath/hg38/ 
#https://genome-test.gi.ucsc.edu/cgi-bin/hgTrackUi?db=hg38&c=chrX&g=cons470way
#download for each species and check md5sum
#only species that wasn’t downloaded was the maritime shrew, I used my protein file from GenSAS and put it in this same directory 

rsync -av rsync://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/003/025/GCF_000003025.6_Sscrofa11.1/GCF_000003025.6_Sscrofa11.1_protein.faa.gz ./
#md5sum 3fbd00e2f87b7c6a84d04a6436bc30d6 
rsync -av rsync://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/296/755/GCF_000296755.1_EriEur2.0/GCF_000296755.1_EriEur2.0_protein.faa.gz ./
#99904107108e78458a7789e40a1ca725
rsync -av rsync://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/002/285/GCF_000002285.5_Dog10K_Boxer_Tasha/GCF_000002285.5_Dog10K_Boxer_Tasha_protein.faa.gz ./
#594d3447bdcb525a90f65576b05c042b 
rsync -av rsync://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/001/405/GCF_000001405.40_GRCh38.p14/GCF_000001405.40_GRCh38.p14_protein.faa.gz ./
#8fd543f5740d64e1a1fd70f5dc305202
rsync -av rsync://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/001/635/GCF_000001635.27_GRCm39/GCF_000001635.27_GRCm39_protein.faa.gz ./
#3b236c220e540359e1ea9d2589d3557c
rsync -av rsync://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/181/275/GCF_000181275.1_SorAra2.0/GCF_000181275.1_SorAra2.0_protein.faa.gz ./
#8d15fde6ac566240f2d91c5e91d64bce
rsync -av rsync://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/024/139/225/GCF_024139225.1_mSunEtr1.pri.cur/GCF_024139225.1_mSunEtr1.pri.cur_protein.faa.gz ./
#6d0e871bb467fb94e8d99e4a0ab17ea3 
rsync -av rsync://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/308/155/GCF_000308155.1_EptFus1.0/GCF_000308155.1_EptFus1.0_protein.faa.gz ./
#9f37e9dcc42ef4da949b780ae15b757e
rsync -av rsync://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/260/355/GCF_000260355.1_ConCri1.0/GCF_000260355.1_ConCri1.0_protein.faa.gz ./
#5a68e3bc2192a8684471abcd1e44b7bb
rsync -av rsync://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/331/955/GCF_000331955.2_Oorc_1.1/GCF_000331955.2_Oorc_1.1_protein.faa.gz  ./
#e0dbcf5937679fa63ee42e4e82ad70be
rsync -av rsync://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/001/905/GCF_000001905.1_Loxafr3.0/GCF_000001905.1_Loxafr3.0_protein.faa.gz  ./
#ce0b70b939e34c8ce1328c87f6bbb5c8
rsync -av rsync://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/151/735/GCF_000151735.1_Cavpor3.0/GCF_000151735.1_Cavpor3.0_protein.faa.gz ./
#96e4a60cc90e07e85083bd3969bdf0e9
rsync -av rsync://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/002/863/925/GCF_002863925.1_EquCab3.0/GCF_002863925.1_EquCab3.0_protein.faa.gz ./
#e95e88bd278973ac11301c6faefe9e4c
rsync -av rsync://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/015/227/675/GCF_015227675.2_mRatBN7.2/GCF_015227675.2_mRatBN7.2_protein.faa.gz ./
#6db9a5d6d91036cc916c0cae190615bd
rsync -av rsync://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/292/845/GCF_000292845.1_OchPri3.0/GCF_000292845.1_OchPri3.0_protein.faa.gz ./
#02715a24925f28efeb6273ffb8d8c6eb 
rsync -av rsync://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/165/445/GCF_000165445.2_Mmur_3.0/GCF_000165445.2_Mmur_3.0_protein.faa.gz  ./
#aa517553feb4944c6f752cf09a52a059
rsync -av rsync://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/325/575/GCF_000325575.1_ASM32557v1/GCF_000325575.1_ASM32557v1_protein.faa.gz  ./
#4186cc1563aeb779934bd84c3ed5d387
rsync -av rsync://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/317/375/GCF_000317375.1_MicOch1.0/GCF_000317375.1_MicOch1.0_protein.faa.gz   ./
#6279d5e770a7b8188a922a3ae126deb9
rsync -av rsync://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/029/834/395/GCF_029834395.1_SorFum_2.1/GCF_029834395.1_SorFum_2.1_protein.faa.gz ./
#dd32359597d90384be6a2694577bc35e

#unzip all of the files
gunzip *gz 

#rename manually all of the files to remove the GCF_####
mv GCF_000292845.1_OchPri3.0_protein.faa  OchPri3.0_protein.faa

#if you want to check the number of proteins in each file you can run this
grep ">" OchPri3.0_protein.faa | wc -l

#for this analysis I only want to keep the longest/primary transcripts
#so make a new directory for those
mkdir primary_transcripts 

#Orthofinder has a tool to only keep longest transcript from each protein file
#run a loop through all the files 
#species with lots of protein transcripts, e.g. humans will still have higher value than other species after this
for f in ./*faa ; do python /home/mlcosset/scratch/genes/OrthoFinder-2.5.4/tools/primary_transcript.py $f ; done

#run OrthoFinder
#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=orthofinder.bash
#SBATCH --mem=0
#SBATCH --cpus-per-task=24
#SBATCH --output=%x_%j
#SBATCH --time=00-23:59# time (DD-HH:MM)
#SBATCH --mail-type=ALL
#SBATCH --mail-user=mcossette@trentu.ca
module load python
pip install scipy
module load StdEnv/2020
module load mafft
module load fasttree
/home/mlcosset/scratch/genes/OrthoFinder-2.5.4/orthofinder.py -f proteomes/primary_transcripts/ -t 24 -M msa -z -S diamond -A mafft -T fasttree -o ./output_oct16

###end!