#Script to identify gene family (orthogroup) size changes using CAFE 
#Main author: Marie-Laurence Cossette
#code is based on resources (listed below)

##########
#Resources
##########

#how to modify OrthoFinder output to use for CAFE https://github.com/harish0201/Analyses_Pipelines/blob/main/7.CAFE.sh
#CAFE tutorial (has the cafetutorial_clade_and_size_filter script) - https://github.com/hahnlab/cafe_tutorial
#https://academic.oup.com/bioinformatics/article/36/22-23/5516/6039105
#good info on lambda models and error rate estimation - http://evomics.org/wp-content/uploads/2016/06/cafe_tutorial-1.pdf

### 1. Prepping files and requirements ###

#download CAFE from the downloaded file on my computer and other requirements
scp Downloads/CAFE-4.2.tar mlcosset@graham.sharcnet.ca:///home/mlcosset/scratch/genes 
tar -xvf CAFE-4.2.tar 
autoconf
./configure
make

#CAFE requires a tree to run
#can visual trees with iTOL
#here I will use the tree output from OrthoFinder, the only thing is we need to change its format
#need to go into the OrthoFinder/tools directory for the script to update tree file format
#use this script to convert SpeciesTree_rooted.txt to ultrametric. To find time of outgroup or root node use TimeTree.org: age-> of root node
module load python
cd ../OrthoFinder-2.5.4/
python ./tools/make_ultrametric.py -r 99.2 ./output_oct16/Results_Oct16/Species_Tree/SpeciesTree_rooted.txt

cd output_oct16/Results_Oct16/Orthogroups
#add null column
awk -F'\t' '{print "(null)\t"$0}' Orthogroups.GeneCount.tsv > tmp.tsv

#add "Desc" to first row first column
#remove _protein after the species names in first row of the tsv file if not cafe wont recognize names, do that for tree too
nano tmp.tsv 

#should look something like this for e.g.
#Desc    Orthogroup      Dog     Human  Cat
#(null)  OG0000064       18      5       9      
#(null)  OG0000065       0       5       0       
#(null)  OG0000066       22      1       13     
#(null)  OG0000067       0       3       0      
#(null)  OG0000068       5       4       0       
#(null)  OG0000069 

#remove the total column that will be the lasyt column in the above file
awk -F'\t' '{$NF=""; print $0}' tmp.tsv | rev | sed 's/^\s*//g' | rev | tr ' ' '\t' > mod.tsv

#go to CAFE directory
cd CAFE-4.2

#copy the edites files required from OrthoFinder
cp /home/mlcosset/scratch/genes/OrthoFinder-2.5.4/output_oct16/Results_Oct16/Orthogroups/mod.tsv ./
cp /home/mlcosset/scratch/genes/OrthoFinder-2.5.4/output_oct16/Results_Oct16/Species_Tree/SpeciesTree_rooted.txt.ultrametric.tree ./

#clone repository for cafe_tutorials because it has a script we need
git clone https://github.com/hahnlab/cafe_tutorial.git

#we need to run the cafetutorial_clade_and_size_filter.py script on the mod.tsv for two reasons
#1.keep only gene families with gene copies in at least two species of the specified clades; this is necessary because ancestral state reconstruction requires at least two species per clade of interest. 
#2.separates gene families with < 100 from > 100 gene copies in one or more species; this is necessary because big gene families cause the variance of gene copy number to be too large and lead to noninformative parameter estimates.
python ./cafe_tutorial/python_scripts/cafetutorial_clade_and_size_filter.py -i mod.tsv -s -o cafe.input.tsv

### 2. running CAFE ###

#we will be running different lambda models in CAFE
#we want to estimate an error model to account for genome assembly error
#error in genome assemblies and gene annotation should artefactually inflate the rate of gene family evolution, and therefore controlling for it should lead to smaller estimates of lambda (λ).

#make reports directory for outputs
mkdir reports

#open nano and make the follwowing file and name it lambda_error.sh 
#basically this has the input info for the CAFE run
#here we are estimating error with only one rate across all species
nano
#!/home/mlcosset/scratch/genes/CAFE-4.2/release/cafe
load -i cafe.input.tsv -t 16 -l reports/log_run_lambda_error.txt
tree (LoxAfr:99.2,((((CanFam:58.6147,EquCab:58.6147):4.51634,((PteAle:50.0982,EptFus:50.0982):11.4938,(OrcOrc:43.794,SusScr:43.794):17.7981):1.53903):3.37026,(ConCri:59.9738,(EriEur:54.8633,((SorCin:12.5953,(SorMar:5.24468,SorAra:5.24468):7.35061):18.5749,SunEtr:31.1702):23.6932):5.11043):6.52755):5.83874,((MicMur:56.9168,Hg38:56.9168):9.97926,((CavPor:55.8575,(MicOch:24.9607,(Mm39:13.5046,RatNor:13.5046):11.4561):30.8968):6.63036,OchPri:62.4878):4.40818):5.44402):26.86)
lambda -s -t (1,((((1,1)1,((1,1)1,(1,1)1)1)1,(1,(1,((1,(1,1)1)1,1)1)1)1)1,((1,1)1,((1,(1,(1,1)1)1)1,1)1)1)1)
report reports/report_run_errors

#run CAFE script to find error rates
#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=CAFE_lambda_error
#SBATCH --mem=0
#SBATCH --cpus-per-task=16
#SBATCH --output=%x_%j
#SBATCH --time=00-02:59# time (DD-HH:MM)
#SBATCH --mail-type=ALL
#SBATCH --mail-user=mcossette@trentu.ca
module load python/2.7
python /home/mlcosset/scratch/genes/CAFE-4.2/cafe_tutorial/caferror.py -i lambda_error.sh -d reports/lambda_error_files -v 0 -f 1

#go look at output
cd reports/lambda_error_files/
cat caferror_default_output.txt

#pick lowest value from second column
#ErrorModel	Score
#0.400390625	419404.188058

#respective distribution can be found in file reports/run6_caferror_files/cafe_errormodel_0.400390625.txt 
#with this file we can now estimate λ values once again

cd ../..

#again make nano file with run info for CAFE
#!cafe
load -i cafe.input.tsv -t 16 -l reports/log_run_n_error.txt
tree (LoxAfr:99.2,((((CanFam:58.6147,EquCab:58.6147):4.51634,((PteAle:50.0982,EptFus:50.0982):11.4938,(OrcOrc:43.794,SusScr:43.794):17.7981):1.53903):3.37026,(ConCri:59.9738,(EriEur:54.8633,((SorCin:12.5953,(SorMar:5.24468,SorAra:5.24468):7.35061):18.5749,SunEtr:31.1702):23.6932):5.11043):6.52755):5.83874,((MicMur:56.9168,Hg38:56.9168):9.97926,((CavPor:55.8575,(MicOch:24.9607,(Mm39:13.5046,RatNor:13.5046):11.4561):30.8968):6.63036,OchPri:62.4878):4.40818):5.44402):26.86)
errormodel -model reports/lambda_error_files/cafe_errormodel_0.400390625.txt -all
lambda -s -t (13,((((7,7)7,((6,6)6,(5,5)5)8)8,(3,(3,((1,(1,1)1)1,2)2)3)3)4,((9,9)9,((11,(10,(10,10)10)10)11,11)11)12)13)
report reports/report_run_multi_best

#now run multi lambda model based on best error rate rerun
#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=CAFE_lambda_multi_best
#SBATCH --mem=0
#SBATCH --cpus-per-task=16
#SBATCH --output=%x_%j
#SBATCH --time=00-11:59# time (DD-HH:MM)
#SBATCH --mail-type=ALL
#SBATCH --mail-user=mcossette@trentu.ca
/home/mlcosset/scratch/genes/CAFE-4.2/release/cafe lambda_multi_best.sh

#use script that summarizes the output from above into tables
cafe_tutorial/python_scripts/cafetutorial_report_analysis.py -i reports/report_run_multi_best.cafe -o reports/summary_multi_best

###end!


