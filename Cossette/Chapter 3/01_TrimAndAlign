#Script to trim and align fastq reads to masked shrew genome 
#Main author: Marie-Laurence Cossette
#code is based on Camille Kessler’s code
https://gitlab.com/WiDGeT_TrentU/graduate_theses/-/blob/eaceadc0434440e6318c01a5c820ed34c89d9be6/Kessler/CH_02/01_mapping.sh

##### Trimming #####
#run FastQC to visualize the quality of sequencing reads
#raw reads are in the fastqs directory 

#01_fastqc.bash 
#!/bin/bash
#SBATCH --tasks-per-node=16
#SBATCH --mem=0G
#SBATCH --time=0-01:59  # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=mcossette@trentu.ca
module load fastqc/0.11.9
fastqc -t 16 -o fastqc_reports fastqs/*

#I have reads from two different sequencing runs with different setting, some samples only have 2 fastqs whereas others have 8 fastqs
#have to work a bit differently for each set in next steps

#create fasta file with nano file containing the adapter sequences
nano
#TruSeq3-PE.fa
>PrefixPE/1
TACACTCTTTCCCTACACGACGCTCTTCCGATCT
>PrefixPE/2
GTGACTGGAGTTCAGACGTGTGCTCTTCCGATCT

#run Trimmomatic, to trim Illumina sequencing data using pretty much default parameters
#the script below is for samples with 8 fastqs
#paired-end mode (PE)
#reads quality scores in phred33 format (-phred33)
#remove adapters (ILLUMINACLIP:TruSeq3-PE.fa:2:30:10)
#^file containing adapter sequences: max # of mismatches allowed: min length of the read after clipping to keep: min length of the adapter present to be considered
#remove leading low quality or N bases (below quality 3) (LEADING:3)
#remove trailing low quality or N bases (below quality 3) (TRAILING:3)
#scan the read with a 4-base wide sliding window, cutting when the average quality per base drops below 15 (SLIDINGWINDOW:4:15)
#drop reads below the 36 bases long (MINLEN:36)

#02_trimmomaticPE_8.bash
#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=trimmomatic_8
#SBATCH --cpus-per-task=16
#SBATCH --mem=0G
#SBATCH --output=%x_%j.out
#SBATCH --mail-user=mcossette@trentu.ca
#SBATCH --mail-type=ALL
#SBATCH --time=0-00:29 # time (DD-HH:MM)
module load StdEnv/2016.4 trimmomatic/0.36
java -jar $EBROOTTRIMMOMATIC/trimmomatic-0.36.jar PE -threads 16 -phred33\
 fastqs/${1}_R1_001.fastq.gz fastqs/${1}_R2_001.fastq.gz\
 trimmed/${1}_trim_R1_001_paired.fastq.gz trimmed/${1}_trim_R1_001_unpaired.fastq.gz\
 trimmed/${1}_trim_R2_001_paired.fastq.gz trimmed/${1}_trim_R2_001_unpaired.fastq.gz\
 ILLUMINACLIP:TruSeq3-PE.fa:2:30:10 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:36

#for each file in the fastqs directory, extract sample name and simplify it, echo and submit trimmomatic script with output directed to trimmed directory and wait 3 seconds between submissions
for f in $(ls fastqs/*.fastq.gz | sed "s|fastqs/||" | sed 's/_R[0-9].*//' | uniq)
do 
	echo ${f}
	sbatch -o trimmed/${f}-%A.out 02_trimmomaticPE_8.bash ${f}  
	sleep 3
 done

#the script below is the same as above but for samples with only 2 fastqs
#could run loop like above if you want, just requires longer runtime

#02_trimmomaticPE_2.bash 
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=trimmomatic_2
#SBATCH --cpus-per-task=16
#SBATCH --mem=0G
#SBATCH --output=%x_%j.out
#SBATCH --mail-user=mcossette@trentu.ca
#SBATCH --mail-type=ALL
#SBATCH --time=0-03:20 # time (DD-HH:MM)
module load StdEnv/2016.4 trimmomatic/0.36
java -jar $EBROOTTRIMMOMATIC/trimmomatic-0.36.jar PE -threads 16 -phred33\
 fastqs/NS11_S94_L005_R1_001.fastq.gz fastqs/NS11_S94_L005_R2_001.fastq.gz\
 trimmed/NS11_S94_L005_trim_R1_001_paired.fastq.gz trimmed/NS11_S94_L005_trim_R1_001_unpaired.fastq.gz\
 trimmed/NS11_S94_L005_trim_R2_001_paired.fastq.gz trimmed/NS11_S94_L005_trim_R2_001_unpaired.fastq.gz\
 ILLUMINACLIP:TruSeq3-PE.fa:2:30:10 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:36

#after the trimming, some of the samples with 2 fastqs still seem to contain polyG tails when running FastQC again
#these can be removed using fastp

#create conda env for fastp
conda create -n fastp
conda activate fastp 
conda install bioconda::fastp 

#use the -g flag to trim polyG tail and only keep reads longer than 36bp
#run this for all of the samples with only 2 fastq files

#03_fastp.bash
#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=fastp
#SBATCH --cpus-per-task=16
#SBATCH --mem=0G
#SBATCH --output=%x_%j.out
#SBATCH --mail-user=mcossette@trentu.ca
#SBATCH --mail-type=ALL
#SBATCH --time=0-00:25 # time (DD-HH:MM)
conda activate fastp
fastp --in1 trimmed/NS9_S92_L005_trim_R1_001_paired.fastq.gz --in2 trimmed/NS9_S92_L005_trim_R2_001_paired.fastq.gz --out1 NS9_S92_L005_trim_R1_001_paired_polyG.fastq.gz --out2 NS9_S92_L005_trim_R2_001_paired_polyG.fastq.gz -g -l 36 -w 16  &> NS9_S92_L005_trim_paired_polyG.log

#moved all of the good trimmed files for samples with 2 and 8 fastqs to the same fastqs_trimmed directory

##### Aligning #####
#index masked shrew genome

#04_bwa-mem2_index.bash  
#!/bin/bash
#SBATCH --mem=80G
#SBATCH --time=0-00:35 # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-user=mcossette@trentu.ca
#SBATCH --mail-type=ALL
module load bwa-mem2
bwa-mem2 index Sorcin_Aug2024_343.fa.gz

#make directory for alignment outputs
mkdir align_outs 

#echo file name
#use bwa-mem2 to align the paired-end reads against the masked shrew genome and include read group information
#pipe output to samtools sort and save output bam to align_outs directory
#run samtools flagstat on the sorted output bam to get alignment statistics
#adjust thread ask, this might be a bit high

#05_bwa-mem2_mem.bash 
#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --cpus-per-task=24
#SBATCH --mem=0G
#SBATCH --time=0-00:59 # time (DD-HH:MM)
#SBATCH --mail-user=mcossette@trentu.ca
#SBATCH --mail-type=ALL
module load bwa-mem2 samtools
echo ${1}
bwa-mem2 mem -t 24 -R "@RG\\tID:${1}\\tSM:${1}\\tLB:${1}\\tPL:ILLUMINA" \
Sorcin_Aug2024_343.fa.gz fastqs_trimmed/${1}_R1_001_paired.fastq.gz fastqs_trimmed/${1}_R2_001_paired.fastq.gz | \
samtools sort -@ 24 -o align_outs/${1}.sorted_reads.bam &&
samtools flagstat align_outs/${1}.sorted_reads.bam > align_outs/${1}.sorted_reads.flagstat

#for each fastq.gz in the fastqs_trimmed directory, extract sample name and simplify it, echo and submit bwa-mem2 script with output directed to aligns_out directory and wait 3 seconds between submissions
for f in $(ls fastqs_trimmed/*.fastq.gz |  sed "s|fastqs_trimmed/||" | sed 's/_R[0-9].*//'| uniq) 
do
	echo ${f}
	sbatch -o align_outs/${f}-%A.out ./05_bwa-mem2_mem.bash ${f}
	sleep 3
done

#run also for polyG trimmed samples but need longer runtime because larger files

#05_bwa-mem2_mem_polyG.bash 
#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --cpus-per-task=24
#SBATCH --mem=0G
#SBATCH --time=0-03:59 # time (DD-HH:MM)
#SBATCH --mail-user=mcossette@trentu.ca
#SBATCH --mail-type=ALL
module load bwa-mem2 samtools
echo ${1}
bwa-mem2 mem -t 24 -R "@RG\\tID:${1}\\tSM:${1}\\tLB:${1}\\tPL:ILLUMINA" \
Sorcin_Aug2024_343.fa.gz fastqs_trimmed/polyG_fastqs/${1}_R1_001_paired.fastq.gz fastqs_trimmed/polyG_fastqs/${1}_R2_001_paired.fastq.gz | \
samtools sort -@ 24 -o align_outs/${1}.sorted_reads.bam &&
samtools flagstat align_outs/${1}.sorted_reads.bam > align_outs/${1}.sorted_reads.flagstat


for f in $(ls fastqs_trimmed/polyG_fastqs/*.fastq.gz |  sed "s|fastqs_trimmed/polyG_fastqs/||" | sed 's/_R[0-9].*//'| uniq) 
do
	echo ${f}
	sbatch -o align_outs/${f}-%A.out ./05_bwa-mem2_mem_polyG.bash ${f}
	sleep 3
done

#want to merge bam files together, this is fine here since they have unique @RG info, if they don’t need to do a step before to include different @RG info to each sample/lane

#run this for each group of bam files for same sample, takes ~30min
#could create a loop but just ran it like this in bg

module load picard/2.26.3
java -jar $EBROOTPICARD/picard.jar MergeSamFiles \
I=ON4_S27_L001_trim.sorted_reads.bam \
I=ON4_S27_L002_trim.sorted_reads.bam \
I=ON4_S27_L003_trim.sorted_reads.bam \
I=ON4_S27_L004_trim.sorted_reads.bam \
O=ON4_S27_merged.bam & 

#MarkDuplicates is important to removing PCR duplicates which can introduce bias in your variant calling. 
#however this might not actually be necessary since we have PCR free library prep?

#run once on the non polyG merged bams and then do polyG ones after, just to make easier for loop and ask time

#echo file name
#use Picard's MarkDuplicates tool to identify and remove duplicate reads from the merged bam files
#run samtools flagstat on the output bam to get alignment statistics using sambamba to be more efficient

#06_picard-dup.bash 
#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --cpus-per-task=8
#SBATCH --mem=128G
#SBATCH --time=0-01:59 # time (DD-HH:MM)
#SBATCH --mail-user=mcossette@trentu.ca
#SBATCH --mail-type=ALL
module load picard samtools sambamba
echo ${1}
java -Xmx120G -jar $EBROOTPICARD/picard.jar MarkDuplicates \
INPUT=${1}_merged.bam OUTPUT=${1}.deduped_reads.bam USE_JDK_DEFLATER=true USE_JDK_INFLATER=true \
ASSUME_SORT_ORDER=coordinate REMOVE_DUPLICATES=true REMOVE_SEQUENCING_DUPLICATES=true METRICS_FILE=${1}.deduped.picard &&
sambamba flagstat --nthreads=30 ${1}.deduped_reads.bam > ${1}.deduped_reads.flagstat

#for each merged.bam in the align_outs directory, extract sample name and simplify it, echo and submit picard-dup script and wait 3 seconds between submissions
for f in $(ls align_outs/*_merged.bam | sed 's/_merged.bam//' | uniq)
do
	echo ${f}
	sbatch -o ${f}_dups-%A.out 06_picard-dup.bash ${f} 
	sleep 3
done

#run again with larger polyG files

#06_picard-dup_polyG.bash 
#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --cpus-per-task=8
#SBATCH --mem=128G
#SBATCH --time=0-05:59 # time (DD-HH:MM)
#SBATCH --mail-user=mcossette@trentu.ca
#SBATCH --mail-type=ALL
module load StdEnv/2020 picard samtools sambamba
echo ${1}
java -Xmx120G -jar $EBROOTPICARD/picard.jar MarkDuplicates \
INPUT=${1}_trim_polyG.sorted_reads.bam  OUTPUT=${1}.deduped_reads.bam USE_JDK_DEFLATER=true USE_JDK_INFLATER=true \
ASSUME_SORT_ORDER=coordinate REMOVE_DUPLICATES=true REMOVE_SEQUENCING_DUPLICATES=true METRICS_FILE=${1}.deduped.picard &&
sambamba flagstat --nthreads=30 ${1}.deduped_reads.bam > ${1}.deduped_reads.flagstat

#rerun with the larger polyG files
for f in $(ls align_outs/*_trim_polyG.sorted_reads.bam | sed 's/_trim_polyG.sorted_reads.bam//' | uniq)
do
	echo ${f}
	sbatch -o ${f}_dups-%A.out 06_picard-dup_polyG.bash ${f} 
	sleep 3
done

#now we want to keep unique reads, specifying we only are keeping reads that mapped to one location and that aren’t tagged SA (chimeric split reads) or XA (secondary hits)
#mapping quality of 0 means its mapping to 5 or more locations so we want quality of 1 or above
#unique reads provide clearer information about the true sequence at each position. If reads are duplicated, it can skew the results, making it harder to distinguish between true variants and artifacts.
#use flagstat again for alignment statistics

#07_unique.bash
#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=unique
#SBATCH --cpus-per-task=8
#SBATCH --mem=12G
#SBATCH --time=0-00:15 # time (DD-HH:MM)
#SBATCH --mail-user=mcossette@trentu.ca
#SBATCH --mail-type=ALL
module load StdEnv/2020 samtools sambamba
echo ${1}
sambamba view --nthreads=8 --with-header --format=bam --show-progress \
-F "mapping_quality >= 1 and not (unmapped or secondary_alignment) and not ([XA] != null or [SA] != null)" \
${1}.deduped_reads.bam -o ${1}.unique_reads.bam &&
sambamba flagstat --nthreads=8 ${1}.unique_reads.bam > ${1}.unique_reads.flagstat

#for each deduped_reads.bam in the align_outs directory, extract sample name and simplify it, echo and submit unique script and wait 3 seconds between submissions
for f in $(ls align_outs/*.deduped_reads.bam | sed 's/.deduped_reads.bam//' | sort -u)
do
	echo ${f}
	sbatch -o ${f}_unique-%A.out 07_unique.bash ${f} 
	sleep 3
done

##### Realigning #####
#marking duplicates doesn’t alter the alignment of reads, but duplicates can affect how well reads align around indels (insertions or deletions)
#realignment helps correct artifacts and improves alignment accuracy around variant sites. 

#index each bam file

#08_bamindex.bash 
#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=bamindex
#SBATCH --mem=12G
#SBATCH --time=0-00:09 # time (DD-HH:MM)
#SBATCH --mail-user=mcossette@trentu.ca
#SBATCH --mail-type=ALL
module load picard/2.23.2  
java -jar $EBROOTPICARD/picard.jar BuildBamIndex I=${1}.unique_reads.bam

#for each unique_reads.bam in the align_outs directory, extract sample name and simplify it, echo and submit bam index script and wait 3 seconds between submissions
for f in $(ls align_outs/*.unique_reads.bam | sed 's/.unique_reads.bam//' | sort -u)
do
	echo ${f}
	sbatch -o ${f}-%A.out 08_bamindex.bash ${f} 
	sleep 3
done

#need to unzip the genome for the next step
gunzip Sorcin_Aug2024_343.fa.gz

#need to create sequence dictionary
module load samtools/1.20
samtools faidx Sorcin_Aug2024_343.fa
module load picard/2.23.2  
java -jar $EBROOTPICARD/picard.jar CreateSequenceDictionary R=Sorcin_Aug2024_343.fa O=Sorcin_Aug2024_343.dict

#RealignerTargetCreator prepares data for local realignment around indels
#scans the alignment data to detect areas where reads are likely misaligned due to the presence of indels, looks at base quality scores and the alignment patterns around potential indel sites
#generates a list of regions (targets) in the genome that are candidates for local realignment where indel-related misalignments are most likely to occur
#IndelRealigner corrects misalignments in sequencing data that occur around indels 
#use flagstat again for alignment statistics

#09_realign.bash 
#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=realign
#SBATCH --cpus-per-task=8
#SBATCH --mem=32G
#SBATCH --time=0-11:59 # time (DD-HH:MM)
#SBATCH --mail-user=mcossette@trentu.ca
#SBATCH --mail-type=ALL
module load StdEnv/2016.4 gatk/3.8 sambamba/0.7.1
echo ${1}
java -Xmx30G -jar $EBROOTGATK/GenomeAnalysisTK.jar -T RealignerTargetCreator --use_jdk_deflater --use_jdk_inflater -nt 8 \
-R Sorcin_Aug2024_343.fa -I ${1}.unique_reads.bam -o ${1}.realign.intervals &&
java -Xmx30G -jar $EBROOTGATK/GenomeAnalysisTK.jar -T IndelRealigner --use_jdk_deflater --use_jdk_inflater \
-R ./Sorcin_Aug2024_343.fa -I ${1}.unique_reads.bam -targetIntervals ${1}.realign.intervals -o ${1}.realigned.bam &&
sambamba flagstat --nthreads=8 ${1}.realigned.bam > ${1}.realigned.flagstat

#for each unique_reads.bam in the align_outs directory, extract sample name and simplify it, echo and submit realign script and wait 3 seconds between submissions
for f in $(ls align_outs/*.unique_reads.bam | sed 's/.unique_reads.bam//' | sort -u)
do
	echo ${f}
	sbatch -o ${f}_realign-%A.out 09_realign.bash ${f} 
	sleep 10
done

#find final coverage using mosdepth
#install with conda
conda install bioconda::mosdepth 

#enter aligns_out directory
cd align_outs 

#index realigned bam files using bg function 
module load samtools 
for f in *.realigned.bam
do 
	echo ${f} 
	samtools index ${f}
	sleep 3
done &

#quick to run so submitted the mosdepth code individually in bg like this for each but could loop
mosdepth -n S3_S88.realigned NS3_S88.realigned.bam &
