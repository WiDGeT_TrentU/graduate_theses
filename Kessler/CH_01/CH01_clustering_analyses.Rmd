---
title: "Clustering analyses"
author: C. Kessler
date: "`r format(Sys.time(), '%d %B %Y')`"
output:
  html_document:
    theme: cosmo
    highlight: kate
    toc: true
    toc_depth: 4
    toc_float: true
    code_folding: show
editor_options: 
  chunk_output_type: console
---

# NGSAdmix

Based on http://www.popgen.dk/software/index.php/NgsAdmixTutorial, analysis on genotype likelihoods (.beagle file)

```{bash echo=T, eval=F}
############ ############ ############ ############ ############ ############ 
# set up directory
module load StdEnv/2016.4 
cd /home/ckessler/projects/rrg-shaferab/ckessler/chapter01
mkdir NGSadmix

## Set paths
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/chapter01/NGSadmix
root=/home/ckessler/projects/rrg-shaferab/ckessler
datadir=/home/ckessler/projects/rrg-shaferab/ckessler/chapter01/data
softwares=/home/ckessler/projects/rrg-shaferab/ckessler/softwares

############ ############ ############ ############ ############ ############ 
## launch NGSAdmix 
# check input file
gunzip -c $datadir/deer_angsd.beagle.gz | head -n 10 | cut -f 1-10 | column -t
gunzip -c $datadir/deer_angsd.beagle.gz | wc -l
## launch NGSAdmix with K=2:7
for i in {2..7}
do
sbatch -o NGSadmix_K${i}.out --account=rrg-shaferab --cpus-per-task 7 --mem 112G --account=rrg-shaferab --job-name=NGSadmix_K${i} --time=0-23:00 --wrap="$softwares/s_NGSadmix/NGSadmix -likes $datadir/deer_angsd.beagle.gz -K ${i} -o ch01_NGSadmix_K${i} -P 7"
echo "ch01_K${i}"
done

# check that the order of the bam_list is the same as sample info
bam=$(cat $datadir/bam_list | awk -F/ '$0=$NF' | sed 's/_S[0-9]*_L00[0-9].*//' | sed 's/_merged.*//')
info=$(cat $datadir/CH01_info.txt | awk '{print $1}')

diff -s <(echo $bam ) <(echo $info) # all good

############ ############ ############ ############ ############ ############ 
## get the data on local 
rsync -rv ckessler@graham.computecanada.ca:/home/ckessler/projects/rrg-shaferab/ckessler/chapter01/NGSadmix/*.qopt /Users/camillekessler/Desktop/Dropbox/PhD/CH_01/Results/Clustering/


rsync -rv ckessler@graham.computecanada.ca:/home/ckessler/projects/rrg-shaferab/ckessler/chapter01/NGSadmix/*.log /Users/camillekessler/Desktop/Dropbox/PhD/CH_01/Results/Clustering/

```


```{r echo=TRUE, message=FALSE, warning=FALSE}
############ ############ ############ ############ ############ ############ 
# import data
# sample info
ch01_info <- read.table("../Data/CH01_info.txt", 
                        sep = "\t", header = TRUE)
# rename for clarity
ch01_info$Genome_ID <- gsub("Odo_", "Ov_", ch01_info$Genome_ID)
ch01_info$Genome_ID <- gsub("MD_|BTD_|SD_", "Oh_", ch01_info$Genome_ID)

# resultst
admixture_files <- list.files(path = "../Results/Clustering",
                              pattern = ".qopt", full.names = TRUE)
# keep only results for both species together
admixture_files_2sp <- grep("_WTD|_MD", x = admixture_files, value = TRUE, 
                            invert = TRUE)
############ ############ ############ ############ ############ ############ 
library(reshape2)
library(ggplot2)
library(easyGgplot2)
p <- list()
for (f in admixture_files_2sp) {
  # Read admixture proportions file
  admixture <- read.table(f)
  # extract K value
  n <- as.numeric(gsub(pattern = ".*_K|.qopt", x = f, replacement = ""))
  # prepare for plot
  admixture$Genome_ID <- ch01_info$Genome_ID # add genomeID
  admixture$group <- ch01_info$Group # add group
  admixture <- melt(admixture, id.vars = c("Genome_ID", "group"), 
                    measure.vars = 1:n) # long format
  admixture$Genome_ID <- factor(admixture$Genome_ID, 
                                levels = unique(admixture$Genome_ID)) #  order
  admixture$group <- factor(admixture$group, 
                            levels = c("WTD_allopatry", "WTD_sympatry",
                                       "MD_sympatry", "MD_allopatry")) # order
  # plot
  p[[n-1]] <- ggplot(admixture, aes(x = Genome_ID, y = value, fill = variable)) +
    geom_bar(stat = "identity") +
    facet_grid(~group, scales = "free_x", space = "free_x") +
    theme_minimal() +
    theme(axis.title.y = element_blank(), axis.title.x = element_blank(),
          axis.text.x = element_text(angle = 45, hjust = 1)) +
    labs(title = paste0("Admixture proportions from K =", n)) +
    guides(fill = "none")
}

pdf(file = "../Results/Plots/ch01_NGSadmix_plot.pdf", width = 12,
    height = 10)
ggplot2.multiplot(plotlist = p, cols = 2)
dev.off()



```


```{bash echo=T, eval=F}
############ ############ ############ ############ ############ ############ 
# find the best k,
# https://github.com/alexkrohn/AmargosaVoleTutorials/blob/master/ngsAdmix_tutorial.md
graham
cd projects/rrg-shaferab/ckessler/chapter01/NGSadmix/
mkdir bestK
cd bestK
datadir=/home/ckessler/projects/rrg-shaferab/ckessler/chapter01/data
softwares=/home/ckessler/projects/rrg-shaferab/ckessler/softwares

############ ############ ############ ############ ############ ############ 
# re-run ngsadmix 10 times for each k
for j in {1..10}
do
for i in {1..7}
do
sbatch -o NGSadmix_K${i}.${j}-%A.out --account=rrg-shaferab --cpus-per-task 7 --mem 112G --account=rrg-shaferab --job-name=NGSadmix_K${i} --time=0-23:30 --wrap="$softwares/s_NGSadmix/NGSadmix -likes $datadir/ch01_deer_angsd.beagle.gz -K ${i} -o ch01_NGSadmix_K${i}.${j} -P 7"
echo "ch01_K${i}.${j}"
done
done

############ ############ ############ ############ ############ ############ 
# create log file
for log in `ls *.log` 
do 
echo $log
grep -Po 'like=\K[^ ]+' $log 
done #> logfile
# 4 didn't work: ch01_NGSadmix_K4.10, ch01_NGSadmix_K6.5 & ch01_NGSadmix_K6.8, K6.9

for log in `ls *.log` 
do 
grep -Po 'like=\K[^ ]+' $log 
done > logfile

############ ############ ############ ############ ############ ############ 
# import
rsync -rvaP ckessler@graham.computecanada.ca:/home/ckessler/projects/rrg-shaferab/ckessler/chapter01/NGSadmix/bestK/logfile /Users/camillekessler/Desktop/Dropbox/PhD/CH_01/Results/Clustering/
```


```{r echo=TRUE, message=FALSE, warning=FALSE}
############ ############ ############ ############ ############ ############
# read log file
logs <- as.data.frame(read.table("../Results/Clustering/logfile"))
# 4 didn't work: ch01_NGSadmix_K4.10, ch01_NGSadmix_K6.5 & ch01_NGSadmix_K6.8, K6.9
logs$K <- c(rep("1", 10), rep("2", 10), rep("3", 10), rep("4", 9), rep("5", 10), 
            rep("6", 7), rep("7", 10))
# randomly remove one values for the others --> based on 7, not 10
new_log <- logs[logs$K == 6, ]
for (i in c(1:5, 7)) {
  tmp <- logs[logs$K == i, ]
  sampl <- sample(1:10, size = 7, replace = FALSE)
  tmp <- tmp[sampl, ]
  new_log <- rbind(new_log, tmp)
}

# export and upload in clumpak
write.table(new_log[, c(2, 1)], "../Results/Clustering/logfile_formatted", 
            row.names = F, col.names = F, quote = F)

# evanno plot for supplementary
evanno <- read.delim("../Results/Clustering/CLUMPAK_output.log", header = FALSE)
evanno <- grep("Delta(K", evanno$V1, value = TRUE, fixed = TRUE)
evanno <- gsub("^.*K=", "", evanno)
evanno <- as.data.frame(evanno)
library(tidyr)
evanno <- separate(evanno, col = evanno, sep = "\\) = ", into = c("K", "deltaK"))
evanno$deltaK <- as.numeric(evanno$deltaK)
evanno$K <- as.numeric(evanno$K)
evanno$group <- "A"

ggplot(evanno, aes(x = K, y = deltaK)) +
  geom_point() +
  geom_line(aes(group = group)) +
  theme_minimal() + 
  scale_y_continuous(labels = comma) +
  labs(y = "Delta K")

ggsave(file = "../Results/Plots/ch01_NGSadmix_evannoPlot.pdf", width = 12, height = 10)
```

# PCAgsd

Based on http://www.popgen.dk/software/index.php/PCAngsdTutorial

PCAngsd handles missing data by "as missing data will be modeled by population structure" - https://www.genetics.org/content/210/2/719#abstract-2 

analysis on genotype likelihoods (beagle file)

```{bash echo=T, eval=F}
############ ############ ############ ############ ############ ############ 
## set up directory
cd /home/ckessler/projects/rrg-shaferab/ckessler/chapter01
mkdir PCAngsd
# set paths
PCAdir=/home/ckessler/projects/rrg-shaferab/ckessler/chapter01/PCAngsd
root=/home/ckessler/projects/rrg-shaferab/ckessler
datadir=/home/ckessler/projects/rrg-shaferab/ckessler/chapter01/data
PCANGSD=/home/ckessler/projects/rrg-shaferab/ckessler/software_pcangsd/pcangsd.py

############ ############ ############ ############ ############ 
## launch PCAngsd based on AF
# If python is not happy (ImportError: libgfortran.so.4: cannot open shared object file: No such file or directory), reinstall pcangsd
module load python

sbatch -o PCAngsd.out --account=rrg-shaferab --cpus-per-task 8 --mem 128G --account=rrg-shaferab --job-name=PCAngsd --time=0-11:00 --wrap="python $PCANGSD -beagle $datadir/deer_angsd.beagle.gz -threads 8 -o ch01_PCAngsd"

## get the data on local 
rsync -rv ckessler@graham.computecanada.ca:/home/ckessler/projects/rrg-shaferab/ckessler/chapter01/PCAngsd/*.cov /Users/camillekessler/Desktop/Dropbox/PhD/CH_01/Results/Clustering/

```


```{r}
############ ############ ############ ############ ############ ############ 
# set up PCA
# read the PCAngsd result, the .cov file
mat <- as.matrix(read.table("../Results/Clustering/ch01_PCAngsd.cov"))
# compute eigenvalues
eig_mat <- eigen(mat) 
# Keep PC1 & 2 in a df
clustering <- as.data.frame(eig_mat$vectors[, 1:2]) 
# bind to the info data
clustering <- cbind(clustering, ch01_info) 
# prepare the shape fo the points
clustering$group_short <- gsub(x = clustering$Group, pattern = "WTD_|MD_", replacement = "")

############ ############ ############ ############ ############ ############ 
# check correlation pearson
# all
cor(clustering$V1, clustering$Lat)
cor(clustering$V1, clustering$Long)
cor(clustering$V2, clustering$Lat)
cor(clustering$V2, clustering$Long)

# MD
corr_MD <- clustering[clustering$Species == "O_hemionus", ]
cor(corr_MD$V1, corr_MD$Lat)
cor(corr_MD$V1, corr_MD$Long)
cor(corr_MD$V2, corr_MD$Lat)
cor(corr_MD$V2, corr_MD$Long)

# WTD
corr_WTD <- clustering[clustering$Species == "O_virginianus", ]
cor(corr_WTD$V1, corr_WTD$Lat)
cor(corr_WTD$V1, corr_WTD$Long)
cor(corr_WTD$V2, corr_WTD$Lat)
cor(corr_WTD$V2, corr_WTD$Long)

cor(clustering$Lat, clustering$Long)

# linear model with latitude and PC 1/2 PC1/2 scores ~ species + lat/long
#PC1 & latitude
m_PC1lat <- lm(clustering$V1 ~ clustering$Species + clustering$Lat)
summary(m_PC1lat)
# PC1 & longitude
m_PC1lon <- lm(clustering$V1 ~ clustering$Species + clustering$Long)
summary(m_PC1lon)
#PC2 & latitude
m_PC2lat <- lm(clustering$V2 ~ clustering$Species + clustering$Lat)
summary(m_PC2lat)
# PC2 & longitude
m_PC2lon <- lm(clustering$V2 ~ clustering$Species + clustering$Long)
summary(m_PC2lon)


############ ############ ############ ############ ############ ############ 
# plot it 
library(ggrepel)
ggplot(clustering, aes(x = V1, y = V2, col = Group)) + 
  geom_point() + 
  theme_minimal() + 
  scale_colour_discrete(breaks = c("O_virginianus", "O_hemionus"), 
                        labels = c("White-tailed deer", "Mule deer")) +  
  geom_label_repel(aes(label = ifelse(V2 < 0.05, Genome_ID, '')), 
                   point.padding = 0.05, segment.color = 'grey50', 
                   max.overlaps = 20)+ 
  theme(legend.title = element_blank(), legend.position = "bottom") +
  labs(title = "PCA based on individual allele frequency")  + 
  # check if eig_mat$values[1]/sum(eig_mat$values)*100 is correct
  xlab(paste('PC1',paste('(',round(eig_mat$values[1]/sum(eig_mat$values)*100),
                         '%)', sep = ""))) +
  ylab(paste('PC2',paste('(',round(eig_mat$values[2]/sum(eig_mat$values)*100),
                         '%)', sep = "")))

ggsave("../Results/Plots/ch01_PCAngsd.pdf")

```


```{r}
sessionInfo()
```

