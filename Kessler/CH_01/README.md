![](../Images/Picture%201.png)

# Speciation without gene-flow in hybridising deer

Code repository for the publication Speciation without gene-flow in hybridising deer, available on [BioRxiv](http://dx.doi.org/10.1101/2022.04.20.488928) and in [Molecular Ecology](https://doi.org/10.1111/mec.16824)

## Table of contents {#table-of-contents}

-   [Abstract](##%20Abstract)

-   [Code repository description](##%20Code%20repository%20description)

## Abstract

[(Back to top)](#table-of-contents)

Under the ecological speciation model, divergent selection acts on ecological differences between populations, gradually creating barriers to gene flow and ultimately leading to reproductive isolation. Hybridisation is part of this continuum and can both promote and inhibit the speciation process. Here, we used white-tailed (*Odocoileus virginianus*) and mule deer (*O. hemionus*) to investigate patterns of speciation in hybridising sister species. We quantified genome-wide introgression and performed genome scans to look for signatures of four different selection scenarios. Despite modern evidence of hybridization, we found no patterns of introgression and no signatures of divergence with gene flow, but localized patterns of allopatric and balancing selection were detected across the genome. Genes under balancing selection were related to immunity, MHC and sensory perception of smell, the latter of which is consistent with deer biology. The deficiency of historical gene-flow suggests that white-tailed and mule deer were spatially separated during the glaciation cycles of the Pleistocene and genome wide differentiation accrued via drift. Dobzhansky-Muller incompatibilities and selection against hybrids are hypothesized to be at play, and evidence suggests these sister species are far along the speciation continuum.

## Code repository description

[(Back to top)](#table-of-contents)

This repository was made for reproducibility purposes.

Description of the scripts in order of execution

-   CH01_sampling.R : R script to select 28 individuals from a pool of 74 according to our sampling scheme. Selected 7 individuals for each species from allopatric zones and 7 from sympatric zones.

-   CH01_ANGSD.sh: ANGSD script for SNP calling from 28 bam files mapped to the WTD genome. The code used to produce the bam files can be found in /Kessler/General_Scripts/mapping_B\*.sh. Includes vcf file formatting for vctools compatibility and renaming of the individuals. Requires the working script 08_ANGSD_ch01.sh

-   CH01_AlignToCaribou.sh: Alignment to caribou genome which will be used as outgroup for the ABBA-BABA analysis, as well as SNP calling on this dataset. Requires working scripts 03_index_caribou.sh, 04_bwa-sort_Caribou.sh and 08_ANGSD_WCaribou.sh.

-   CH01_clustering_analyses.Rmd: Rmarkdown containing bash & R code for the clustering analyses (NGSadmix & PCAngsd)

-   CH01_genome_scans.Rmd: Rmarkdown containing bash & R code for the genome scans analysis. Requires the working script genome_scans_species.sh

-   CH01_ABBABABA.Rmd: Rmarkdown containing bash & R code for the ABBA-BABA analysis

-   CH01_Treemix.Rmd: Rmarkdown containing bash & R code for the treemix analysis

-   CH01_recombination_rate.Rmd: Rmarkdown containing bash & R code for the recombination rate analysis and their correlation to $F_{ST}$ and $\pi$

-   working_scripts

    -   08_ANGSD_ch01.sh: Bash script to start ANGSD for samples mapped to WTD

    -   03_index_caribou.sh: Bash script to index the Caribou genome

    -   04_bwa-sort_Caribou.sh: Bash script to mapping samples to Caribou

    -   08_ANGSD_WCaribou.sh: Bash script to start ANGSD for samples mapped to Caribou

    -   genome_scans_species.sh: Bash script to start the genome scans analysis using popgenWindows.py

MSMC-IM analysis was performed by A. Shafer following several tutorials:

    - Phasing from https://github.com/jessicarick/msmc2_scripts/blob/master/msmc_1.5_phase.sh

    - https://github.com/stschiff/msmc-tools/blob/master/msmc-tutorial/guide.md
        
    - https://github.com/pickettbd/msmc-slurmPipeline
