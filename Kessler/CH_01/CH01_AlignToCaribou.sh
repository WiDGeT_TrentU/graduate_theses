# Alignement to caribou genome

# For ABBA BABA we need an outgroup, here we chose the caribou as closest related species to the _Odocoileus_ genera. For analyses using the outgroup, we need to map our samples to a caribou genome: rangifer_tarandus_QC.fa in $HOME/projects/rrg-shaferab/MAMMAL_GENOMES. Our caribou sample is GAP-157, all provided by Aaron on Graham (see slack 17.09.2021).

########## ########## ########## ########## ########## ########## ########## 
# set up directory
root=/home/ckessler/projects/rrg-shaferab/ckessler
ch01=/home/ckessler/projects/rrg-shaferab/ckessler/chapter01
datadir=/home/ckessler/projects/rrg-shaferab/ckessler/chapter01/data
cd $ch01
# mkdir Caribou_data
cd Caribou_data
mv $root/GAP* . # bam was provided by Aaron, see slack 17.09.2021
# get the reference
rsync -va $HOME/projects/rrg-shaferab/MAMMAL_GENOMES/rangifer_tarandus_QC.fa ch01_alignedCaribou
# index it
cp $root/03_index.sh 03_index_caribou.sh # adapt it with nano
sbatch -o indexing.out 03_index_caribou.sh 

########## ########## ########## ########## ########## ########## ########## 
#	first, sort by read name (necessary for bwa to work)
cd $ch01
# mkdir data_alignedCaribou
cd data_alignedCaribou
#mkdir slurms
module load StdEnv/2016.4
module load samtools

while read line
do
f=$(echo $line | sed "s|^.*mapping_final_bam/||" | sed 's/.realigned.bam//')
echo ${f}
sbatch -o slurms/${f}_sort_bam-%A.out --account=rrg-shaferab --time=11:00:00 --cpus-per-task 4 --mem=16G --job-name=sort_bam --wrap="samtools sort -n -@ 4 -o ${f}_realigned_sorted.bam ${line}"
done < $datadir/bam_list

########## ########## ########## ########## ########## ########## ########## 
# then, convert final bams to fastqs
for f in $(ls *.bam | sed -r 's/_realigned_.*$//') 
do
echo ${f}
sbatch -o slurms/${f}_to_fq-%A.out --account=rrg-shaferab --time=11:00:00 --cpus-per-task 4 --mem=16G --job-name=convertfastq --wrap="samtools fastq -@ 4 -s ${f}_singleton.fq -1 ${f}_realigned_sorted_R1.fq -2 ${f}_realigned_sorted_R2.fq ${f}_realigned_sorted.bam"
sleep 10
done

########## ########## ########## ########## ########## ########## ########## 
#	bwa_sort 
# copy code and adapt with nano
cp $root/04_bwa-sortSE.sh 04_bwa-sort_Caribou.sh
nano 04_bwa-sort_Caribou.sh

for f in $(ls *.fq | sed -r 's/_realigned_.*$//' | uniq) 
do
echo ${f}

ref=/home/ckessler/projects/rrg-shaferab/ckessler/chapter01/Caribou_data
cariboudir=/home/ckessler/projects/rrg-shaferab/ckessler/chapter01/data_alignedCaribou

sbatch -o ${f}_alignCaribou-%A.out 04_bwa-sort_Caribou.sh ${f} ${ref}/rangifer_tarandus_QC.fa
sleep 10
done

########## ########## ########## ########## ########## ########## ########## 
# run ANGSD with the all samples plus the caribou 
cp $datadir/08_ANGSD_ch01.sh 08_ANGSD_WCaribou.sh # copy ANGSD script$
nano 08_ANGSD_WCaribou.sh # change bam list & output name
ls *realign_caribou.sorted_reads.bam > bam_list_caribou # make the bam list
echo $ch01/Caribou_data/GAP-157.realigned.bam >> bam_list_caribou # append the caribou  
cat bam_list_caribou # check

# check it's the same number and names of scaffolds
samtools view -H $ch01/Caribou_data/GAP-157.realigned.bam | tail 
samtools view -H SD_AK1_S18_L004_realign_caribou.sorted_reads.bam | tail 

sbatch -o ANGSD_caribou.out 08_ANGSD_WCaribou.sh 

############ ############ ############ ############ ############
# edit vcf file header
# We change from ##fileformat=VCFv4.2(angsd version) to ##fileformat=VCFv4.2
module load StdEnv/2016.4
module load plink
gunzip -c ch01_angsd_mapped_caribou.vcf.gz | head -n 1

# create header
echo "##fileformat=VCFv4.2" > header.vcf

#insert new first line with: ##fileformat=VCFv4.2
# remove 1st line
sbatch -o format.out --cpus-per-task 1 --mem=4G --account=rrg-shaferab --time=04:00:00 --job-name=zip1 --wrap="zcat ch01_angsd_mapped_caribou.vcf.gz | sed 1d  > temp.vcf"
# add new header
sbatch -o format.out --cpus-per-task 1 --mem=4G --account=rrg-shaferab --time=04:00:00 --job-name=zip2 --wrap="cat header.vcf temp.vcf > ch01_angsd_mapped_caribou_vcfCompatible.vcf"
# zip
sbatch -o zip.out --cpus-per-task 1 --mem 4G --account=rrg-shaferab --time=15:00:00 --job-name=zip3 --wrap="gzip ch01_angsd_mapped_caribou_vcfCompatible.vcf"

## rename individuals in VCF --> from IND01 to Odo_,..
module load bcftools

cat bam_list_caribou | awk -F/ '$0=$NF' | sed 's/_realign_caribou.sorted_reads.bam//' | sed 's/_S[[:digit:]]\+_L00.//' > Samples_list.txt # list with order of samples IDs

# rehaeder in bcftools
sbatch -o indv_named.out --cpus-per-task 1 --mem 4G --account=rrg-shaferab --time=15:00:00 --job-name=indv_named --wrap="bcftools reheader -s Samples_list.txt ch01_angsd_mapped_caribou_vcfCompatible.vcf.gz -o ch01_angsd_mapped_caribou_vcfCompatible_named.vcf.gz"

# check that it worked
module load bcftools
bcftools query -l ch01_angsd_mapped_caribou_vcfCompatible_named.vcf.gz
gunzip -c ch01_angsd_mapped_caribou_vcfCompatible_named.vcf.gz  | head -n 10 | cut -f 1-10 | column -t

