#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=genome_scan
#SBATCH --cpus-per-task=3
#SBATCH --mem=2G
#SBATCH --time=0-11:00 # time (DD-HH:MM)

module load python
module load scipy-stack


python popgenWindows.py -w 50000 -m 500 -g ${1} -o ${2}_popFreq.csv --writeFailedWindows --windType coordinate -f pairs -T 3 --analysis popFreq -p WTD -p MD --popsFile pops_species.txt

python popgenWindows.py -w 50000 -m 500 -g ${1} -o ${2}_popDist.csv --writeFailedWindows --windType coordinate -f pairs -T 3 --analysis popDist -p WTD -p MD --popsFile pops_species.txt

python popgenWindows.py -w 50000 -m 500 -g ${1} -o ${2}_popPairDist.csv --writeFailedWindows --windType coordinate -f pairs -T 3 --analysis popPairDist -p WTD -p MD --popsFile pops_species.txt

python popgenWindows.py -w 50000 -m 500 -g ${1} -o ${2}_indPairDist.csv --writeFailedWindows --windType coordinate -f pairs -T 3 --analysis indPairDist -p WTD -p MD --popsFile pops_species.txt

python popgenWindows.py -w 50000 -m 500 -g ${1} -o ${2}_indHet.csv --writeFailedWindows --windType coordinate -f pairs -T 3 --analysis indHet -p WTD -p MD --popsFile pops_species.txt