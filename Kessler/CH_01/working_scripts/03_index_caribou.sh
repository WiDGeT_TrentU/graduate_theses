#!/bin/bash
#SBATCH --time=0-16:00
#SBATCH --account=rrg-shaferab
#SBATCH --mem=8G

module load StdEnv/2016.4
module load bwa
module load picard
module load gatk/3.8
module load samtools
java -Xmx8G -jar $EBROOTPICARD/picard.jar CreateSequenceDictionary R=${1}/wtdgenome1.fasta O=${1}/wtdgenome1.dict
samtools faidx ${1}/wtdgenome1.fasta
bwa index ${1}/wtdgenome1.fasta -a bwtsw