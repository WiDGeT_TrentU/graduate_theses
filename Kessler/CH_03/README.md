![](https://i.redd.it/zcb1lszerry71.jpg)

# Ancient DNA of the Toronto subway deer adds to the extinction list of Ice Age megafauna

Code repository for the publication _Ancient DNA of the Toronto subway deer adds to the extinction list of Ice Age megafauna_, available on BioRxiv

## Table of contents

- [Abstract](## Abstract)

- [Repository description](## Repository description)
  

## Abstract
[(Back to top)](#table-of-contents)

The late Pleistocene was a time of global megafaunal extinctions that were particularly severe in North America. The continent lost many mammal taxa, but the validity of several remain ambiguous, including, a high proportion of Cervidae taxa. Torontoceros hypogaeus is represented by a single specimen (ROMM75974) discovered in 1976 during excavation work for the Toronto subway in Canada. The species was described based on its unique antler morphology, but the variable nature of that trait and the species near absence in the fossil record leads to uncertainty concerning its systematic relationships. We used ancient DNA to clarify the taxonomic placement and evolutionary history of T. hypogaeus. We performed mitochondrial and whole genome analyses with related cervids and showed that ROMM75974 has a close affinity, but relatively high divergence from the Odocoileus sister species. ROMM75974 could represent a distinct Odocoileus species to be included in the list of extinct North American taxa. Based on antler morphology, this species was likely adapted to open landscape; we hypothesise that its preferred habitat changed rapidly at the end of the Pleistocene, highlighting the role of climate change in the extinction of megafauna biodiversity at the end of the ice age.

## Repository description

[(Back to top)](#table-of-contents)

This repository was made for reproducibility purposes, the full script for all analyses is provided in an .Rmd file and corresponding html.

- ch03-clean: Complete code for the publication, includes data preparation, phylogenetic analyses and divergence analyses.

- data-files: All data required for phylogenetic analyses, includes alignments and xml files.


