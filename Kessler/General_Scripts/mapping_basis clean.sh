# Script for the mapping of Deer samples in Graham
# template for future mappings
# From FastQC to MultiQC

##########################################################################################################
#	SETUP
##########################################################################################################
# path to fastq
fastq=/home/ckessler/projects/rrg-shaferab/ckessler/...

# path to references
ref=/home/ckessler/projects/rrg-shaferab/ckessler/...

# path to analysis directory
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/...

# root
root=/home/ckessler/projects/rrg-shaferab/ckessler



##########################################################################################################
#	PREP 
##########################################################################################################

# index the genome
sbatch -o indexing.out $root/03_index.sh $ref # you can find the adapted 03_index.sh in /Users/camillekessler/Desktop/PhD/Scripts/adapted_Resequencing_SOP

# going to split up the genome into scaffolds to help with run - do before submitting job
mkdir $analysisdir/mpileup_run
awk 'BEGIN {FS="\t"}; {print $1 FS "0" FS $2}' $ref/wtdgenome1.fasta.fai > mpileup_run/mpileup_coord.bed
awk 'BEGIN {FS="\t"}; {print $1 FS "1" FS $2}' $ref/wtdgenome1.fasta.fai > mpileup_run/mpileup_coord.list

# Store the slurms in different directories:
mkdir $analysisdir/fastqc_slurm 
mkdir $analysisdir/trimmed_slurm
mkdir $analysisdir/align_slurm
mkdir $analysisdir/dups_slurm
mkdir $analysisdir/unique_slurm
mkdir $analysisdir/clean_slurm

# get sambamba
wget https://github.com/biod/sambamba/releases/download/v0.7.0/sambamba-0.7.0-linux-static.gz
gunzip sambamba-0.7.0-linux-static.gz

##########################################################################################################
#	FastQC 
##########################################################################################################
cd $analysisdir
module load StdEnv/2016.4
module load fastqc
for f in `ls $fastq/*.fastq.gz`
do

ID=$(echo ${f} | sed "s|$fastq/||" | sed 's/_R[0-9].*//')
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/...

echo ${f}
echo ${ID}
sbatch -o $analysisdir/fastqc_slurm/${ID}-%A.out --account=rrg-shaferab --time=02:00:00 --job-name=fastq --wrap="fastqc ${f} -o ${analysisdir}"
sleep 10
done




##########################################################################################################
#	Trimmomatic 
##########################################################################################################
for f in $(ls $fastq/*.fastq.gz | sed "s|$fastq/||" | sed 's/_R[0-9].*//' | uniq)
do
echo ${f}
fastq=/home/ckessler/projects/rrg-shaferab/ckessler/...
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/...

sbatch -o $analysisdir/trimmed_slurm/${f}-%A.out $root/02_trimmomaticPE.sh ${f} ${analysisdir} ${fastq}
sleep 10
done



##########################################################################################################
#	bwa_sort 
##########################################################################################################

for f in $(ls $fastq/*.fastq.gz |  sed "s|$fastq/||" | sed 's/_R[0-9].*//'| uniq) # change cut -f1-4 -d'_' didn't work with Odo_ON_X2_S12_L004_R1_001
do
echo ${f}

fastq=/home/ckessler/projects/rrg-shaferab/ckessler/...
ref=/home/ckessler/projects/rrg-shaferab/ckessler/...
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/...

sbatch -o $analysisdir/align_slurm/${f}-%A.out $root/04_bwa-sortSE.sh ${f} ${ref}/wtdgenome1.fasta ${fastq} ${analysisdir}
sleep 10
done


##########################################################################################################
#	picard
##########################################################################################################

for f in $(ls *.bam | sed 's/.sorted_reads.bam//' | sort -u)
do
echo ${f}
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/...
root=/home/ckessler/projects/rrg-shaferab/ckessler

sbatch -o $analysisdir/dups_slurm/${f}-%A.out $root/05_picard-dup.sh ${f} ${analysisdir} ${root}
sleep 10
done

##########################################################################################################
#	Unique
##########################################################################################################

for f in $(ls *.deduped_reads.bam | sed 's/.deduped_reads.bam//' | sort -u)
do
echo ${f}
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/...
root=/home/ckessler/projects/rrg-shaferab/ckessler

sbatch -o $analysisdir/unique_slurm/${f}-%A.out $root/06_unique.sh ${f} ${analysisdir} ${root}
sleep 10
done


##########################################################################################################
#	realign
##########################################################################################################

for f in $(ls *.unique_reads.bam | sed 's/.unique_reads.bam//' | sort -u)
do
echo ${f}

ref=/home/ckessler/projects/rrg-shaferab/ckessler/GENOME_files
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/...
root=/home/ckessler/projects/rrg-shaferab/ckessler

sbatch -o $analysisdir/clean_slurm/${f}-%A.out $root/07_realign.sh ${f} ${ref} ${analysisdir} ${root}

sleep 10
done


########
# check, should be 20 each
########
for f in $(ls *.unique_reads.bam | sed 's/.unique_reads.bam//' | sort -u)
do 

echo ${f}
ls ${f}* | wc

done

##########################################################################################################
#	Mosdepth
##########################################################################################################
# installation
wget https://github.com/brentp/mosdepth/releases/download/v0.3.1/mosdepth
chmod 764 mosdepth
./mosdepth -h

# final coverage
for f in $(ls *.realigned.bam | sed 's/.realigned.bam//' | sort -u)
do
echo ${f}

analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/...
root=/home/ckessler/projects/rrg-shaferab/ckessler

sbatch -o $analysisdir/mosdepth_slurm/${f}-%A.out --mem=4G --account=rrg-shaferab --time=02:00:00 --job-name=mosdepth --wrap="$root/mosdepth --no-per-base ${f} ${f}.realigned.bam"

sleep 10
done



##########################################################################################################
#	MultiQC
##########################################################################################################
# installation

module load python/3.6
virtualenv --no-download ~/ENV # create a virtual environment,
source ~/ENV/bin/activate # activate it
pip install --no-index --upgrade pip # upgrade pip
pip install multiqc # install 
deactivate # exit environment
/home/ckessler/ENV/bin/multiqc -f .


rsync -rv ckessler@graham.computecanada.ca:/home/ckessler/projects/rrg-shaferab/ckessler/.../multiqc* /Users/camillekessler/Desktop/Dropbox/PhD/Data













