# ANGSD script
# SNP calling of Deer samples in Graham
# Include vcf file formatting for vctools compatibility


################################################################################
#	ANGSD
# for all samples
################################################################################
root=/home/ckessler/projects/rrg-shaferab/ckessler
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/DATA_ANGSD

cd $analysisdir/
ls

## samples that were merged and for which we only keep the merged and downsampled files from batches 10 & 11
# Batch 10: BTD_WA1, MD_NM2, Odo_Key3, Odo_Key5
# Batch 11: Odo_ON6, Odo_QC2, 

# first, create a list of the aligned bam files we don't want to include 
# (bam files from the first sequencing round of each samples that were merged)
ls $root/mapping_final_bam | grep -E 'BTD_WA1|MD_NM2|Odo_Key3|Odo_Key5|Odo_ON6|Odo_QC2' | grep "_L0**" > to_remove_bam
# then create the bam files list wihtout above samples
ls $root/mapping_final_bam/*| grep -vf to_remove_bam > bam_list 

############ ############ ############ ############ ############
# launch ANGSD
sbatch -o $analysisdir/ANGSD.out $root/08_ANGSD.sh

############ ############ ############ ############ ############
#Editing angsd vcf to be compatible with further steps and vcftools we need a new header called vcf_head
#We change from ##fileformat=VCFv4.2(angsd version) to ##fileformat=VCFv4.2
module load StdEnv/2016.4
module load plink
gunzip -c $analysisdir/ch01_deer_angsd.vcf.gz  | head -n 1

# header we want
echo "##fileformat=VCFv4.2" > header.vcf
# remove header we don't want
sbatch -o format.out --cpus-per-task 1 --mem=4G --account=rrg-shaferab --time=04:00:00 --job-name=zip1 --wrap="zcat ch01_deer_angsd.vcf.gz | sed 1d  > temp.vcf"
# cat them together
sbatch -o format.out --cpus-per-task 1 --mem=4G --account=rrg-shaferab --time=04:00:00 --job-name=zip2 --wrap="cat header.vcf temp.vcf > ch01_deer_angsd_compatible.vcf"
# re-zip
sbatch -o zip.out --cpus-per-task 1 --mem 4G --account=rrg-shaferab --time=15:00:00 --job-name=zip3 --wrap="gzip ch01_deer_angsd_compatible.vcf"

############ ############ ############ ############ ############
## rename individuals in VCF --> from IND01 to Odo_,..
module load StdEnv/2016.4
module load bcftools

# list with order of samples IDs
cat bam_list | awk -F/ '$0=$NF' | sed 's/.realigned.bam//' | sed 's/_S[[:digit:]]\+_L00.//' > Samples_list.txt 

# rehaeder in bcftools
sbatch -o indv_named.out --cpus-per-task 1 --mem 4G --account=rrg-shaferab --time=15:00:00 --job-name=indv_named --wrap="bcftools reheader -s Samples_list.txt ch01_deer_angsd_compatible.vcf.gz -o  ch01_deer_angsd_compatible_named.vcf.gz"

# check that it worked
bcftools query -l ch01_deer_angsd_compatible_named.vcf.gz
gunzip -c ch01_deer_angsd_compatible_named.vcf.gz  | head -n 10 | cut -f 1-10 | column -t 


################################################################################
#	ANGSD
# for WTD samples only
################################################################################
root=/home/ckessler/projects/rrg-shaferab/ckessler
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/DATA_ANGSD
cd $analysisdir/

# create the bam_list by modyfing the general one
cat bam_list | grep -E "Odo_|WTD_" > bam_list_WTD

# copy ANGSD script and change bam file, output name &nThreads
cp $root/08_ANGSD.sh ./08_ANGSD_WTD.sh
nano 08_ANGSD_WTD.sh

############### ############### ############### ###############
# launch ANGSD
sbatch -o $analysisdir/ANGSD_WTD.out 08_ANGSD_WTD.sh


################################################################################
#	ANGSD
# for 21 MD only
################################################################################
# create the bam_list by modyfing the general one
cat bam_list | grep -vE "Odo_|WTD_" > bam_list_MD

# copy ANGSD script and change bam file, output name & time limit
cp 08_ANGSD_WTD.sh 08_ANGSD_MD.sh
nano 08_ANGSD_MD.sh
# launch ANGSD
sbatch -o $analysisdir/ANGSD_MD.out 08_ANGSD_MD.sh
