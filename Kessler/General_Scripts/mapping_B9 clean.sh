# Script for the mapping of Deer samples in Graham
# Batch 9 comprises 7 samples: MD_MT2, Odo_Key1, Odo_Key3, Odo_Key4, Odo_Key5, Odo_MT1, Odo_NS1
# From FastQC to ...

##########################################################################################################
#	SETUP
##########################################################################################################
# path to fastq
fastq=/home/ckessler/projects/rrg-shaferab/ckessler/DATA_FASTQ_Batch9

# path to references
ref=/home/ckessler/projects/rrg-shaferab/ckessler/GENOME_files

# path to analysis directory
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B9

# root
root=/home/ckessler/projects/rrg-shaferab/ckessler

##########################################################################################################
#	PREP 
#########################################################################################################

# going to split up the genome into scaffolds to help with run - do before submitting job
mkdir $analysisdir/mpileup_run
awk 'BEGIN {FS="\t"}; {print $1 FS "0" FS $2}' $ref/wtdgenome1.fasta.fai > mpileup_run/mpileup_coord.bed
awk 'BEGIN {FS="\t"}; {print $1 FS "1" FS $2}' $ref/wtdgenome1.fasta.fai > mpileup_run/mpileup_coord.list

# Store the slurms in different directories:
mkdir $analysisdir/fastqc_slurm 
mkdir $analysisdir/trimmed_slurm
mkdir $analysisdir/align_slurm
mkdir $analysisdir/dups_slurm
mkdir $analysisdir/unique_slurm
mkdir $analysisdir/clean_slurm
mkdir $analysisdir/mosdepth_slurm

##########################################################################################################
#	FastQC 
# 10.05, 14:20
##########################################################################################################
cd $analysisdir
module load StdEnv/2016.4
module load fastqc
for f in `ls $fastq/*.fastq.gz`
do

ID=$(echo ${f} | sed "s|$fastq/||" | sed 's/_R[0-9].*//')
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B9

echo ${f}
echo ${ID}
sbatch -o $analysisdir/fastqc_slurm/${ID}-%A.out --account=rrg-shaferab --time=02:00:00 --job-name=fastq --wrap="fastqc ${f} -o ${analysisdir}"
sleep 10
done


##########################################################################################################
#	Trimmomatic 
# 10.05, 15:50
##########################################################################################################
for f in $(ls $fastq/*.fastq.gz | sed "s|$fastq/||" | sed 's/_R[0-9].*//' | uniq)
do
echo ${f}
fastq=/home/ckessler/projects/rrg-shaferab/ckessler/DATA_FASTQ_Batch9
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B9

sbatch -o $analysisdir/trimmed_slurm/${f}-%A.out $root/02_trimmomaticPE.sh ${f} ${analysisdir} ${fastq}
sleep 10
done



##########################################################################################################
#	bwa_sort 
# 11.05 08:30
##########################################################################################################

for f in $(ls $fastq/*.fastq.gz |  sed "s|$fastq/||" | sed 's/_R[0-9].*//'| uniq) 
do
echo ${f}

fastq=/home/ckessler/projects/rrg-shaferab/ckessler/DATA_FASTQ_Batch9
ref=/home/ckessler/projects/rrg-shaferab/ckessler/GENOME_files
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B9

sbatch -o $analysisdir/align_slurm/${f}-%A.out $root/04_bwa-sortSE.sh ${f} ${ref}/wtdgenome1.fasta ${fastq} ${analysisdir}
sleep 10
done


##########################################################################################################
#	picard
# 11.05, 15h
##########################################################################################################

for f in $(ls *.bam | sed 's/.sorted_reads.bam//' | sort -u)
do
echo ${f}
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B9
root=/home/ckessler/projects/rrg-shaferab/ckessler

sbatch -o $analysisdir/dups_slurm/${f}-%A.out $root/05_picard-dup.sh ${f} ${analysisdir} ${root}
sleep 10
done

##########################################################################################################
#	Unique
# 11.05, 17h
##########################################################################################################

for f in $(ls *.deduped_reads.bam | sed 's/.deduped_reads.bam//' | sort -u)
do
echo ${f}
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B9
root=/home/ckessler/projects/rrg-shaferab/ckessler

sbatch -o $analysisdir/unique_slurm/${f}-%A.out $root/06_unique.sh ${f} ${analysisdir} ${root}
sleep 10
done


##########################################################################################################
#	realign
# 11.05, 17:25
##########################################################################################################

for f in $(ls *.unique_reads.bam | sed 's/.unique_reads.bam//' | sort -u)
do
echo ${f}

ref=/home/ckessler/projects/rrg-shaferab/ckessler/GENOME_files
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B9
root=/home/ckessler/projects/rrg-shaferab/ckessler

sbatch -o $analysisdir/clean_slurm/${f}-%A.out $root/07_realign.sh ${f} ${ref} ${analysisdir} ${root}

sleep 10
done


########
# check, should be 20 each
########
for f in $(ls *.unique_reads.bam | sed 's/.unique_reads.bam//' | sort -u)
do 

echo ${f}
ls ${f}* | wc

done


##########################################################################################################
#	Mosdepth
##########################################################################################################
# final coverage
for f in $(ls *.realigned.bam | sed 's/.realigned.bam//' | sort -u)
do
echo ${f}

analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B9
root=/home/ckessler/projects/rrg-shaferab/ckessler

sbatch -o $analysisdir/mosdepth_slurm/${f}-%A.out --mem=4G --account=rrg-shaferab --time=02:00:00 --job-name=mosdepth --wrap="$root/mosdepth --no-per-base ${f} ${f}.realigned.bam"

sleep 10
done



##########################################################################################################
#	MultiQC
##########################################################################################################
/home/ckessler/ENV/bin/multiqc -f .


rsync -rv ckessler@graham.computecanada.ca:/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B9/multiqc* /Users/camillekessler/Desktop/Dropbox/PhD/Data/multiqc_B9





