################################################################################
# Fastq samples mapping
################################################################################

# All pipeline performed by C.Kessler

############ ############ ############ ############ ############
#	SETUP
# root
root=/home/ckessler/projects/rrg-shaferab/ckessler
# path to fastq
fastq=$root/DATA_FASTQ
# path to references
ref=$root/GENOME_files
# path to analysis directory
analysisdir=$root/mapping


############ ############ ############ ############ ############
#	PREP 
# going to split up the genome into scaffolds to help with run - do before submitting job
mkdir $analysisdir/mpileup_run
awk 'BEGIN {FS="\t"}; {print $1 FS "0" FS $2}' $ref/wtdgenome1.fasta.fai > mpileup_run/mpileup_coord.bed
awk 'BEGIN {FS="\t"}; {print $1 FS "1" FS $2}' $ref/wtdgenome1.fasta.fai > mpileup_run/mpileup_coord.list

# Store the slurms in different directories:
mkdir $analysisdir/fastqc_slurm 
mkdir $analysisdir/trimmed_slurm
mkdir $analysisdir/align_slurm
mkdir $analysisdir/dups_slurm
mkdir $analysisdir/unique_slurm
mkdir $analysisdir/clean_slurm
mkdir $analysisdir/downsample_slurm

############ ############ ############ ############ ############
#	FastQC 
cd $analysisdir
module load StdEnv/2016.4 fastqc/0.11.9
# loop for each fastq
for f in `ls $fastq/*.fastq.gz`
do
	# sample ID
	ID=$(echo ${f} | sed "s|$fastq/||" | sed 's/_R[0-9].*//')
	echo ${f}
	echo ${ID}
	# launch fastqc
	sbatch -o $analysisdir/fastqc_slurm/${ID}-%A.out --account=rrg-shaferab --time=02:00:00 --job-name=fastq --wrap="fastqc ${f} -o ${analysisdir}"
	sleep 10
done

# check it worked
tail -n 1 fastqc_slurm/*


############ ############ ############ ############ ############
#	Trimmomatic 
for f in $(ls $fastq/*.fastq.gz | sed "s|$fastq/||" | sed 's/_R[0-9].*//' | uniq)
do
	echo ${f}
	sbatch -o $analysisdir/trimmed_slurm/${f}-%A.out $root/02_trimmomaticPE.sh ${f} ${analysisdir} ${fastq}
	sleep 10
done

## 02_trimmomaticPE.sh ##
#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=trimmomatic
#SBATCH --cpus-per-task=16
#SBATCH --mem=8G
#SBATCH --time=0-06:00 # time (DD-HH:MM)

module load StdEnv/2016.4 trimmomatic/0.36

java -jar $EBROOTTRIMMOMATIC/trimmomatic-0.36.jar PE -threads 16 -phred33 ${3}/${1}_R1_001.fastq.gz ${3}/${1}_R2_001.fastq.gz  ${2}/${1}_trim_R1_001.fastq.gz ${2}/${1}_NAtrim_R1_001.fastq.gz ${2}/${1}_trim_R2_001.fastq.gz ${2}/${1}_NAtrim_R2_001.fastq.gz ILLUMINACLIP:TruSeq3-PE.fa:2:30:10 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:36
## END ##


############ ############ ############ ############ ############
#	bwa_sort 
for f in $(ls $fastq/*.fastq.gz |  sed "s|$fastq/||" | sed 's/_R[0-9].*//'| uniq) 
do
	echo ${f}
	sbatch -o $analysisdir/align_slurm/${f}-%A.out $root/04_bwa-sortSE.sh ${f} ${ref}/wtdgenome1.fasta ${fastq} ${analysisdir}
	sleep 10
done

## 04_bwa-sortSE.sh ###
#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=bwa-bam-sort
#SBATCH --cpus-per-task=16
#SBATCH --mem=64000
#SBATCH --time=0-11:00 # time (DD-HH:MM)

module load StdEnv/2016.4 bwa/0.7.17 samtools/1.10
echo ${1}

bwa mem -M -t 16 -R "@RG\\tID:${1}\\tSM:${1}\\tLB:${1}\\tPL:ILLUMINA" \
${2} ${4}/${1}_trim_R1_001.fastq.gz ${4}/${1}_trim_R2_001.fastq.gz | \
samtools sort -@ 16 -o ${4}/${1}.sorted_reads.bam &&
samtools flagstat ${4}/${1}.sorted_reads.bam > ${4}/${1}.sorted_reads.flagstat
## END ##

############ ############ ############ ############ ############
#	picard add read groups for the samples to merge

# samples with several sequencing runs
string="Odo_ON6_S1_L001 Odo_ON6_S12_L008 Odo_QC2_S5_L001 Odo_QC2_S6_L001 BTD_WA1_S8_L008 BTD_WA1_S7_L001 MD_NM2_S7_L008 MD_NM2_S4_L001 Odo_Key3_S5_L007 Odo_Key3_S3_L001 Odo_Key5_S6_L007 Odo_Key5_S5_L001"

module load picard/2.23.2 samtools/1.10 sambamba/0.7.1 

for f in $(echo $string | sed 's/ /\n/g')
do
	# sample ID
	ID=$(echo ${f} | awk -F/ '$0=$NF')
	# if you find mapping_B11
	if echo "$f" | grep -q "mapping_B11"
		# then it's the second sequencing run 
		then 
			# newfile name with path to analysisdir
			new_ID=$(echo $analysisdir/${ID}_rgA2) 
			# read group name
			RG="2" 
		# otherwise it's the first	
		else 
			# newfile name with path to analysisdir
			new_ID=$(echo $analysisdir/${ID}_rgA1)
			# read group name 
			RG="1" 
		fi
		# library name
		lib=$(echo ${f} | sed -E 's/[^L]+//') 
		echo ${ID}
		# replace read groups
		sbatch -o $analysisdir/dups_slurm/${ID}_addRG-%A.out --account=rrg-shaferab --time=02:00:00 --mem 4GB --job-name=add_RG --wrap="java -jar $EBROOTPICARD/picard.jar AddOrReplaceReadGroups I=${f}.sorted_reads.bam  O=${new_ID}.sorted_reads.bam RGID=${RG} RGPU=unit1 RGLB=${lib} RGSM=20 RGPL=ILLUMINA SORT_ORDER=coordinate CREATE_INDEX=True"
done

############ ############ ############ ############ ############
#	picard merge bam files for samples to merge

# samples to merge
string="Odo_ON6_S1_L001,Odo_ON6_S12_L008 Odo_QC2_S5_L001,Odo_QC2_S6_L001 BTD_WA1_S8_L008,BTD_WA1_S7_L001 MD_NM2_S7_L008,MD_NM2_S4_L001 Odo_Key3_S5_L007,Odo_Key3_S3_L001 Odo_Key5_S6_L007,Odo_Key5_S5_L001"

for f in $(echo $string | sed 's/ /\n/g')
do
	# read group 1 bam file
	input1=$(echo ${f} | sed 's/^.*,//') 
	echo $input1
	# read group 2 bam file
	input2=$(echo ${f} | sed 's/,.*$//') 
	echo $input2
	# sample ID
	ID=$(echo ${f} | sed 's/_S.*$//') 
	echo $ID
	# merge bam files
	sbatch -o $analysisdir/dups_slurm/${ID}_merge-%A.out --account=rrg-shaferab --cpus-per-task 4 --mem 16GB --time=11:00:00 --job-name=merge --wrap="java -jar $EBROOTPICARD/picard.jar MergeSamFiles I=${input1}_rgA1.sorted_reads.bam \
      I=${input2}_rgA2.sorted_reads.bam O=${ID}_merged.sorted_reads.bam"

done

############ ############ ############ ############ ############
#	picard remove dups for all samples 
for f in $(ls *.sorted_reads.bam | sed 's/.sorted_reads.bam//' | uniq)
do
	echo ${f}
	sbatch -o $analysisdir/dups_slurm/${f}_dups-%A.out $root/05_picard-dup.sh ${f} ${analysisdir} ${root}
	sleep 10
done

## 05_picard-dup.sh ##
#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=remove-dups
#SBATCH --cpus-per-task=8
#SBATCH --mem=128G
#SBATCH --time=0-04:00 # time (DD-HH:MM)

module load StdEnv/2016.4 picard/2.23.2
module load samtools/1.10 sambamba/0.7.1

echo ${1}
java -Xmx120G -jar $EBROOTPICARD/picard.jar MarkDuplicates \
INPUT=${2}/${1}.sorted_reads.bam OUTPUT=${2}/${1}.deduped_reads.bam USE_JDK_DEFLATER=true USE_JDK_INFLATER=true \
ASSUME_SORT_ORDER=coordinate REMOVE_DUPLICATES=true REMOVE_SEQUENCING_DUPLICATES=true METRICS_FILE=${2}/${1}.deduped.picard &&
${3}/sambamba-0.7.0-linux-static flagstat --nthreads=30 ${2}/${1}.deduped_reads.bam > ${2}/${1}.deduped_reads.flagstat
## END ##

############ ############ ############ ############ ############
#	Unique
for f in $(ls *.deduped_reads.bam | sed 's/.deduped_reads.bam//' | sort -u)
do
	echo ${f}
	sbatch -o $analysisdir/unique_slurm/${f}-%A.out $root/06_unique.sh ${f} ${analysisdir} ${root}
	sleep 10
done

## 06_unique.sh ###
#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=unique
#SBATCH --cpus-per-task=30
#SBATCH --mem=12G
#SBATCH --time=0-01:00 # time (DD-HH:MM)

module load StdEnv/2016.4 samtools/1.10 sambamba/0.7.1
echo ${1}
${3}/sambamba-0.7.0-linux-static view --nthreads=30 --with-header --format=bam --show-progress \
-F "mapping_quality >= 1 and not (unmapped or secondary_alignment) and not ([XA] != null or [SA] != null)" \
${2}/${1}.deduped_reads.bam -o ${2}/${1}.unique_reads.bam &&
${3}/sambamba-0.7.0-linux-static flagstat --nthreads=30 ${2}/${1}.unique_reads.bam > ${2}/${1}.unique_reads.flagstat
## END ##


############ ############ ############ ############ ############
#	local realignment

for f in $(ls *.unique_reads.bam | sed 's/.unique_reads.bam//' | sort -u)
do
	echo ${f}
	sbatch -o $analysisdir/clean_slurm/${f}-%A.out $root/07_realign.sh ${f} ${ref} ${analysisdir} ${root}
	sleep 10
done


## 07_realign.sh ##
#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=realign
#SBATCH --cpus-per-task=8
#SBATCH --mem=32G
#SBATCH --time=0-12:00 # time (DD-HH:MM)

module load StdEnv/2016.4 gatk/3.8 sambamba/0.7.1
echo ${1}
java -Xmx30G -jar $EBROOTGATK/GenomeAnalysisTK.jar -T RealignerTargetCreator --use_jdk_deflater --use_jdk_inflater -nt 8 \
-R ${2}/wtdgenome1.fasta -I ${3}/${1}.unique_reads.bam -o ${3}/${1}.realign.intervals &&
java -Xmx30G -jar $EBROOTGATK/GenomeAnalysisTK.jar -T IndelRealigner --use_jdk_deflater --use_jdk_inflater \
-R ${2}/wtdgenome1.fasta -I ${3}/${1}.unique_reads.bam -targetIntervals ${3}/${1}.realign.intervals -o ${3}/${1}.realigned.bam &&
${4}/sambamba-0.7.0-linux-static flagstat --nthreads=8 ${3}/${1}.realigned.bam > ${3}/${1}.realigned.flagstat
## END ##


############ ############ ############ ############ ############
#	Downsample merged samples for regular analysis

# Odo_ON6_merged initial mosdepth coverage = 18, need to downsample of 22% (4.1*100/18)
sbatch -o $analysisdir/downsample_slurm/Odo_ON6-%A.out --cpus-per-task 2 --mem=8G --account=rrg-shaferab --time=06:00:00 --job-name=downsample --wrap="java -Xmx120G -jar $EBROOTPICARD/picard.jar DownsampleSam I=Odo_ON6_merged.realigned.bam O=Odo_ON6_merged_dowsampled.realigned.bam P=0.22"

# BTD_WA1_merged mosdepth coverage = 22, need to downsample to 18% (4.1*100/22)
sbatch -o $analysisdir/downsample_slurm/BTD_WA1-%A.out --cpus-per-task 2 --mem=8G --account=rrg-shaferab --time=06:00:00 --job-name=downsample --wrap="java -Xmx120G -jar $EBROOTPICARD/picard.jar DownsampleSam I=BTD_WA1_merged.realigned.bam O=BTD_WA1_merged_downsampled.realigned.bam P=0.18"

# MD_NM2_merged	mosdepth coverage = 17, need to downsample to 24% (4.1*100/17)
sbatch -o $analysisdir/downsample_slurm/MD_NM2-%A.out --cpus-per-task 2 --mem=8G --account=rrg-shaferab --time=06:00:00 --job-name=downsample --wrap="java -Xmx120G -jar $EBROOTPICARD/picard.jar DownsampleSam I=MD_NM2_merged.realigned.bam O=MD_NM2_merged_downsampled.realigned.bam P=0.24"

# Odo_Key5_merged mosdepth coverage = 15, need to downsample to 27% (4.1*100/15)
sbatch -o $analysisdir/downsample_slurm/Odo_Key5-%A.out --cpus-per-task 2 --mem=8G --account=rrg-shaferab --time=06:00:00 --job-name=downsample --wrap="java -Xmx120G -jar $EBROOTPICARD/picard.jar DownsampleSam I=Odo_Key5_merged.realigned.bam O=Odo_Key5_merged_downsampled.realigned.bam P=0.27"


############ ############ ############ ############ ############
#	Mosdepth
# final coverage
for f in $(ls *.realigned.bam | sed 's/.realigned.bam//' | sort -u)
do
	echo ${f}
	sbatch -o $analysisdir/mosdepth_slurm/${f}-%A.out --mem=4G --account=rrg-shaferab --time=02:00:00 --job-name=mosdepth --wrap="$root/mosdepth --no-per-base ${f} ${f}.realigned.bam"
	sleep 10
done

############ ############ ############ ############ ############
#	MultiQC
/home/ckessler/ENV/bin/multiqc -f --cl_config log_filesize_limit:2000000000 . # increased max file size



################################################################################
# ANGSD script
# SNP calling of Deer samples in Graham
# for filtered samples --> no large coverage, total = 73
################################################################################
root=/home/ckessler/projects/rrg-shaferab/ckessler
analysisdir=/home/ckessler/scratch/ch02/data
data=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_final_bam

############ ############ ############ ############ ############
# create bam lists
ls *.realigned.bam > bam_list
cat bam_list | grep -vE "Odo_|WTD_" | grep -Ev "_merged.rea|MD_BC1" > bam_list_MD_filter # remove merged and MD_BC1
cat bam_list | grep -E "Odo_|WTD_" | grep -v "[5-6]_merged.rea" > bam_list_WTD_filter # remove merged
cat bam_list_WTD_filter bam_list_MD_filter > bam_list_filter

############ ############ ############ ############ ############
# launch ANGSD
module load StdEnv/2016.4 angsd/0.918
input=$analysisdir/bam_list_filter
output=$analysisdir/deer_filtered 
nthreads=4

sbatch -o slurm/ANGSD_filtered-%A.out --cpus-per-task 4 --mem=92G --time=3-00:00:00 $root/08_ANGSD_repo.sh ${input} ${output} ${nthreads}

## 08_ANGSD_repo.sh ##
#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=angsd

angsd -bam ${1} -doMajorMinor 1 -domaf 1 -out ${2} -doVcf 1 -nThreads ${3} -doGeno 4 -doPost 1 -gl 2 -SNP_pval 1e-6 -minMapQ 20 -minQ 20 -doCounts 1 -skipTriallelic 1 -doGlf 2
#note the VCF output is 4.2 and can't be read by vcftools as is; continuing through until phasing will produce a readable VCF
## END ##


############ ############ ############ ############ ############
# Prepare hardcalls vcf file
# we will use the geno file (hardcalls) to produce a hardcalls vcf

### step 1: create header
header1="#CHROM POS" # col 1 and 2
header2=$(cat $input | awk -F/ '$0=$NF' | sed 's/_S[0-9]*_L00[0-9].*//' | sed 's/_merg.*$//' | tr '\n' '\t') # sample names
# put them together
echo -e $header1"\t"$header2 | sed 's/ \+/\t/g' | gzip > header.gz 
# cat the header before geno file
sbatch -o slurm/reheader-%A.out --cpus-per-task 1 --account=rrg-shaferab --time=04:00:00 --job-name=reheader --wrap="zcat header.gz $output.geno.gz | gzip -k > $output-named.geno.gz"

### step2: convert to vcf
soft=$root/softwares
ref=$root/GENOME_files/wtdgenome1.fasta.gz
module load python/3.8.10
# from https://github.com/simonhmartin/genomics_general/tree/master/VCF_processing
sbatch -o slurm/geno2vcf-%A.out --account=rrg-shaferab --mem=8000M --cpus-per-task=1 --time=23:59:00 --job-name=geno2vcf --wrap="python $soft/genoToVCF.py -g $output-named.geno.gz -f pairs -o $output-named_hardcalls.vcf -r $ref" 


### step 3: filter vcf
# get samples ID per species
cat bam_list_WTD_filter | awk -F/ '$0=$NF' | sed 's/.realigned.bam//' | sed 's/_S[[:digit:]]\+_L00.//' > wtd-indiv.txt 
cat bam_list_WTD_filter | awk -F/ '$0=$NF' | sed 's/.realigned.bam//' | sed 's/_S[[:digit:]]\+_L00.//' > md-indiv.txt 
# filter per species
module load vcftools/0.1.16
for sp in md wtd 
do 
sbatch -o slurm/split-$sp-%A.out --account=rrg-shaferab --cpus-per-task=1 --time=02:59:00 --job-name=splitvcf --wrap="vcftools --vcf $output-named_hardcalls.vcf --keep $sp-indiv.txt --max-alleles 2 --recode --stdout | gzip -c > $sp-filtered-named_biallelic_hardcalls.vcf.gz"
done
# for the general, only biallelic
sbatch -o slurm/split-$output-%A.out --account=rrg-shaferab --cpus-per-task=1 --time=02:59:00 --job-name=splitvcf --wrap="vcftools --vcf $output-named_hardcalls.vcf --max-alleles 2 --recode --stdout | gzip -c > $output-filtered-named_biallelic_hardcalls.vcf.gz"
# unzip it all
for f in $(ls *_biallelic_hardcalls.vcf.gz)
do
	sbatch -o slurm/unzip_$f-%A.out --cpus-per-task 1 --mem=1G --time=11:59:00 --account=rrg-shaferab --wrap="gunzip -k $f"
done
	


################################################################################
#	ANGSD
# for filtered WTD samples
# total = 53
################################################################################
input=$analysisdir/bam_list_WTD_filter
output=$analysisdir/WTD_filtered_angsd 
nthreads=8
sbatch -o ANGSD_WTD_filter-A%.out --account=rrg-shaferab --job-name=angsd --mem 128G --cpus-per-task 8 --time=00-72:00 $root/08_ANGSD_repo.sh ${input} ${output} ${nthreads} 



################################################################################
#	ANGSD
# for filtered MD samples 
# total = 20
################################################################################
input=$analysisdir/bam_list_MD_filter
output=$analysisdir/MD_filtered_angsd 
nthreads=8
sbatch -o ANGSD_MD_filter-A%.out --account=rrg-shaferab --job-name=angsd --mem 32G --cpus-per-task 8 --time=00-36:00 $root/08_ANGSD_repo.sh ${input} ${output} ${nthreads} 





