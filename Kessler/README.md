![](Kessler/Images/deer_ppt_template.png)

# Speciation genomics of the _Odocoileus_ genera



## Table of contents

- [Project description](##Project-description)
  - [Chapter 1 - Speciation genomics](https://gitlab.com/WiDGeT_TrentU/graduate_theses/-/tree/master/Kessler/CH_01?ref_type=heads)
  
  - [Chapter 2 - Demographic history](https://gitlab.com/WiDGeT_TrentU/graduate_theses/-/tree/master/Kessler/CH_02?ref_type=heads)

  - [Chapter 3 - Ancient DNA of _Torontoceros hypogaeus_](https://gitlab.com/WiDGeT_TrentU/graduate_theses/-/tree/master/Kessler/CH_03?ref_type=heads)
  

## Project description
[(Back to top)](#table-of-contents)

White-tailed deer (_Odocoileus virginianus_; WTD) and mule deer (_O. hemionus_; MD) are two deer sister species endemic to the Americas. They are similar on many aspects of their ecology and life history and are highly abundant in North America, with a density impacting vegetation and predator-prey dynamics. They also represent a high economic value as deer hunting-related activities, and an important cultural component of Indigenous communities across the whole range of the species. 

### Chapter 1 - Speciation without gene-flow in hybridizing deer

[(Back to top)](#table-of-contents)

Hybridisation of WTD and MD is highly documented, those two species can reproduce and create fertile hybrids which does not fit in the biological species concept. With modern data from 28 individuals randomly sampled in areas of sympatry and allopatry, we aim to understand the hybridisation of WTD and MD, as well as find signs of genetic isolation and divergence. 

Published in Molecular Ecology: https://doi.org/10.1111/mec.16824


### Chapter 2 - Demographic history
[(Back to top)](#table-of-contents)

The complex climatic cycles of the Pleistocene, concluding with the LGM, generally forced species to move from a wide range into smaller, often isolated refugia. The peopling of North America and the colonisation by Europeans several millennia later represented further pressures to many populations inhabiting the continent . Here, with a dataset of 73 deer covering most of the North American range, we want to understand the consequences of climatic and anthropogenic pressures on WTD & MD populations through explicit demographic modelling and selection analyses. 

Published in Moleculat Biology and Evolution: http://dx.doi.org/10.1093/molbev/msae038


### Chapter 3 - Ancient DNA of _Torontoceros hypogaeus_
[(Back to top)](#table-of-contents)

The museum specimen ROMM75794 is the only representent of the species _Torontoceros hypogaeus_ which was described in 1982. ROMM75794 was discovered during excavation work for the Toronto subway, and as the antler morphology of this specimen does not fit that of any other North American Cervidae, the authors described it as a new species. Here we use aDNA analysis to investigate the taxonomic placement of ROMM75794 within Cervidae and explore whether the species needs to be added to the list of extinct Pleistocene megafauna.

