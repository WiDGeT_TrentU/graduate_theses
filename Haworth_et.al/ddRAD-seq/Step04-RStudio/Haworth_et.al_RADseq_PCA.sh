# Used tutorial and scripts from https://botany.natur.cuni.cz/hodnocenidat/Lesson_05_tutorial.pdf

## Installing required packages ------------
install.packages("vcfR")
install.packages("adegenet")
install.packages("adegraphics")
install.packages("pegas")
install.packages("StAMPP")
install.packages("lattice")
install.packages("gplots")
install.packages("ape")
install.packages("ggmap")

## Loading required packages ------------
library(vcfR)
library(adegenet)
library(adegraphics)
library(pegas)
library(StAMPP)
library(lattice)
library(gplots)
library(ape)
library(ggmap)

## Improrting files
vcf <- read.vcfR("pop3.r090.populations.snps.vcf.gz")

## Convert to genlight object
aa.genlight <- vcfR2genlight(vcf, n.cores=80)

## Add real SNP.names
locNames(aa.genlight) <- paste(vcf@fix[,1],vcf@fix[,2],sep="_")

## Add population names, here the group names are the first 3 characters of the ind name
pop(aa.genlight) <- substr(indNames(aa.genlight),1,3)

## Check the genlight and its basic information
aa.genlight

## Check individual names
indNames(aa.genlight)

## Check population assignment
pop(aa.genlight)

## Look at data matrix
## glPlot (aa.genlight)

## Number of missing SNPs per samples
x <- summary(t(as.matrix(aa.genlight)))
write.table(x[7,], file = "RADseq_filt_missing.persample.txt", sep = "\t")

## Allele Frequency Distribution Plot
mySum <- glSum(aa.genlight, alleleAsUnit = TRUE)
pdf("RADseq_filt_AlleleFreqPlot.pdf", height = 11, width = 8.5)
AlleleFreqPlot <- barplot(table(mySum), col="blue", space=0, xlab="Allele counts",
                          main="Allele Frequency Distribution for Ontario White-tailed Deer")
dev.off()


###################### start function code ######################
glPcaFast <- function(x,
                      center=TRUE,
                      scale=FALSE,
                      nf=NULL,
                      loadings=TRUE,
                      alleleAsUnit=FALSE,
                      returnDotProd=FALSE){
  if(!inherits(x, "genlight")) stop("x is not a genlight object")
  # keep the original mean / var code, as it's used further down
  # and has some NA checks..
  if(center) {
    vecMeans <- glMean(x, alleleAsUnit=alleleAsUnit)
    if(any(is.na(vecMeans))) stop("NAs detected in the vector of means")
  }
  if(scale){
    vecVar <- glVar(x, alleleAsUnit=alleleAsUnit)
    if(any(is.na(vecVar))) stop("NAs detected in the vector of variances")
  }
  # convert to full data, try to keep the NA handling as similar
  # to the original as possible
  # - dividing by ploidy keeps the NAs
  mx <- t(sapply(x$gen, as.integer)) / ploidy(x)
  # handle NAs
  NAidx <- which(is.na(mx), arr.ind = T)
  if (center) {
    mx[NAidx] <- vecMeans[NAidx[,2]]
  } else {
    mx[NAidx] <- 0
  }
  # center and scale
  mx <- scale(mx,
              center = if (center) vecMeans else F,
              scale = if (scale) vecVar else F)
  # all dot products at once using underlying BLAS
  # to support thousands of samples, this could be
  # replaced by 'Truncated SVD', but it would require more changes
  # in the code around
  allProd <- tcrossprod(mx) / nInd(x) # assume uniform weights
  ## PERFORM THE ANALYSIS ##
  ## eigenanalysis
  eigRes <- eigen(allProd, symmetric=TRUE, only.values=FALSE)
  rank <- sum(eigRes$values > 1e-12)
  eigRes$values <- eigRes$values[1:rank]
  eigRes$vectors <- eigRes$vectors[, 1:rank, drop=FALSE]
  ## scan nb of axes retained
  if(is.null(nf)){
    barplot(eigRes$values, main="Eigenvalues", col=heat.colors(rank))
    cat("Select the number of axes: ")
    nf <- as.integer(readLines(n = 1))
  }
  ## rescale PCs
  res <- list()
  res$eig <- eigRes$values
  nf <- min(nf, sum(res$eig>1e-10))
  ##res$matprod <- allProd # for debugging
  ## use: li = XQU = V\Lambda^(1/2)
  eigRes$vectors <- eigRes$vectors * sqrt(nInd(x)) # D-normalize vectors
  res$scores <- sweep(eigRes$vectors[, 1:nf, drop=FALSE],2,
                      sqrt(eigRes$values[1:nf]), FUN="*")
  ## GET LOADINGS ##
  ## need to decompose X^TDV into a sum of n matrices of dim p*r
  ## but only two such matrices are represented at a time
  if(loadings){
    if(scale) {
      vecSd <- sqrt(vecVar)
    }
    res$loadings <- matrix(0, nrow=nLoc(x), ncol=nf) # create empty matrix
    ## use: c1 = X^TDV
    ## and X^TV = A_1 + ... + A_n
    ## with A_k = X_[k-]^T v[k-]
    myPloidy <- ploidy(x)
    for(k in 1:nInd(x)){
      temp <- as.integer(x@gen[[k]]) / myPloidy[k]
      if(center) {
        temp[is.na(temp)] <- vecMeans[is.na(temp)]
        temp <- temp - vecMeans
      } else {
        temp[is.na(temp)] <- 0
      }
      if(scale){
        temp <- temp/vecSd
      }
      res$loadings <- res$loadings + matrix(temp) %*% eigRes$vectors[k,
                                                                     1:nf, drop=FALSE]
    }
    res$loadings <- res$loadings / nInd(x) # don't forget the /n of X_tDV
    res$loadings <- sweep(res$loadings, 2, sqrt(eigRes$values[1:nf]),
                          FUN="/")
  }
  ## FORMAT OUTPUT ##
  colnames(res$scores) <- paste("PC", 1:nf, sep="")
  if(!is.null(indNames(x))){
    rownames(res$scores) <- indNames(x)
  } else {
    rownames(res$scores) <- 1:nInd(x)
  }
  if(!is.null(res$loadings)){
    colnames(res$loadings) <- paste("Axis", 1:nf, sep="")
    if(!is.null(locNames(x)) & !is.null(alleles(x))){
      rownames(res$loadings) <- paste(locNames(x),alleles(x), sep=".")
    } else {
      rownames(res$loadings) <- 1:nLoc(x)
    }
  }
  if(returnDotProd){
    res$dotProd <- allProd
    rownames(res$dotProd) <- colnames(res$dotProd) <- indNames(x)
  }
  res$call <- match.call()
  class(res) <- "glPca"
  return(res)
}
###################### end function code ######################

pca.1 <- glPcaFast(aa.genlight, nf=300)

## Exporting PCA information
pca.1.eig <- as.matrix(pca.1$eig)
write.table(pca.1.eig, file = "RADseq_filt_pca1_eig.txt", sep = "\t")

pca.1.scores <- as.matrix(pca.1$scores)
write.table(pca.1.scores, file = "RADseq_filt_pca1_scores.txt", sep = "\t")

## Loadings takes a few seconds longer since its huge
pca.1.loadings <- as.matrix(pca.1$loadings)
write.table(pca.1.loadings, file = "RADseq_filt_pca1_loadings.txt", sep = "\t")

# proportion of explained variance by first three axes
pca.1$eig[1]/sum(pca.1$eig) # proportion of variation explained by 1st axis
## 0.02518977
pca.1$eig[2]/sum(pca.1$eig) # proportion of variation explained by 2nd axis
## 0.01461917
pca.1$eig[3]/sum(pca.1$eig) # proportion of variation explained by 3rd axis
## 0.01202869

pdf("RADseq_filt_scatter_pc1_v_pc2.pdf", height = 11, width = 8.5)
scatter.glPca(pca.1, xax = 1, yax = 2, label = NULL) + geom_point(aes(shape=19))
dev.off()

pdf("RADseq_filt_scatter_pc1_v_pc3.pdf", height = 11, width = 8.5)
scatter.glPca(pca.1, xax = 1, yax = 3)
dev.off()

pdf("RADseq_filt_scatter_pc2_v_pc3.pdf", height = 11, width = 8.5)
scatter.glPca(pca.1, xax = 2, yax = 3)
dev.off()


## plot eigenvalues
pdf("RADseq_filt_eigenvalues.pdf", height = 11, width = 8.5)
barplot(pca.1$eig, main="eigenvalues", col=heat.colors(length(pca.1$eig)))
dev.off()
## Save Figure
pdf("RADseq_filt_PCASNPs_ax13.pdf", width=11, height=8.5)
col <- funky(5)
g1 <- s.class(pca.1$scores, pop(aa.genlight), xax=1, yax=3,
              col=transp(col,.6),
              ellipseSize=0, starSize=0, ppoints.cex=4, paxes.draw=T,
              pgrid.draw =F, plot = FALSE)
g2 <- s.label (pca.1$scores, xax=1, yax=3, ppoints.col = "red", plabels =
                 list(box = list(draw = FALSE),
                      optim = TRUE), paxes.draw=T, pgrid.draw =F, plabels.cex=1, plot = FALSE)
ADEgS(c(g1, g2), layout = c(1, 2))
dev.off()

## Save Figure
pdf("RADseq_filt_PCASNPs_ax23.pdf", width=11, height=8.5)
col <- funky(5)
g1 <- s.class(pca.1$scores, pop(aa.genlight), xax=2, yax=3,
              col=transp(col,.6),
              ellipseSize=0, starSize=0, ppoints.cex=4, paxes.draw=T,
              pgrid.draw =F, plot = FALSE)
g2 <- s.label (pca.1$scores, xax=2, yax=3, ppoints.col = "red", plabels =
                 list(box = list(draw = FALSE),
                      optim = TRUE), paxes.draw=T, pgrid.draw =F, plabels.cex=1, plot = FALSE)
ADEgS(c(g1, g2), layout = c(1, 2))
dev.off()

# save fig
pdf("RADseq_filt_PCASNPs_ax12.pdf", width=11, height=8.5)
col <- funky(5)
g1 <- s.class(pca.1$scores, pop(aa.genlight), xax=1, yax=2,
              col=transp(col,.6),
              ellipseSize=0, starSize=0, ppoints.cex=4, paxes.draw=T,
              pgrid.draw =F, plot = FALSE)
g2 <- s.label (pca.1$scores, xax=1, yax=2, ppoints.col = "red", plabels =
                 list(box = list(draw = FALSE),
                      optim = TRUE), paxes.draw=T, pgrid.draw =F, plabels.cex=1, plot = FALSE)
ADEgS(c(g1, g2), layout = c(1, 2))
dev.off()

####### PC1 regressions with lat / long
pca.1$scores[,1]
coords <- read.csv(file="20200528_RADseq_metadata.csv", header = T, na.strings = c("n/a"), sep = "\t")
PC1lat <- lm(pca.1$scores[,1]~coords$Lat)
PC1long <- lm(pca.1$scores[,1]~coords$Long)
PC2lat <- lm(pca.1$scores[,2]~coords$Lat)
PC2long <- lm(pca.1$scores[,2]~coords$Long)

## Exporting the lms
PC1vlat <- as.matrix(summary(PC1lat))
write.table(PC1vlat, file = "RADseq_filt_lm_summary_PC1vlat.txt", sep = "\t")

PC1vlong <- as.matrix(summary(PC1long))
write.table(PC1vlong, file = "RADseq_filt_lm_summary_PC1vlong.txt", sep = "\t")

PC2vlat <- as.matrix(summary(PC2lat))
write.table(PC2vlat, file = "RADseq_filt_lm_summary_PC2vlat.txt", sep = "\t")

PC2vlong <- as.matrix(summary(PC2long))
write.table(PC2vlong, file = "RADseq_filt_lm_summary_PC2vlong.txt", sep = "\t")

pdf("RADseq_filt_scatter_PC1vlat.pdf", height = 11, width = 8.5)
plot(pca.1$scores[,1],coords$Lat)
dev.off()

pdf("RADseq_filt_scatter_PC1vlong.pdf", height = 11, width = 8.5)
plot(pca.1$scores[,1],coords$Long)
dev.off()

pdf("RADseq_filt_scatter_PC2vlat.pdf", height = 11, width = 8.5)
plot(pca.1$scores[,2],coords$Lat)
dev.off()

pdf("RADseq_filt_scatter_PC2vlong.pdf", height = 11, width = 8.5)
plot(pca.1$scores[,2],coords$Long)
dev.off()

PC1 <- pca.1$scores[,1]
PC2 <- pca.1$scores[,2]
LAT <- coords$Lat
LONG <- coords$Long

gg.pca <- data.frame(PC1,LAT,LONG)
attach(gg.pca)

library(ggplot2)
theme_set(theme_bw()) 
gg1 <- ggplot(gg.pca, aes(x=LONG, y=PC1)) + 
  geom_point() + 
  geom_smooth(method="lm", se=F) + 
  labs( 
    y="PC1", 
    x="Longitude", 
    title = bquote(~ R^2 *'= 0.7573')) ## CHANGE ME ACCORDING TO R^2 VALUES summary(PC1long)
## plot(gg1)
ggsave(filename = "RADseq_filt_PC1vlong.png", plot = gg1, device = "png", scale = 2, width = 8.5, height = 11, units = c("cm"), dpi = 600, limitsize = FALSE)

gg2 <- ggplot(gg.pca, aes(x=LAT, y=PC1)) + 
  geom_point() + 
  geom_smooth(method="lm", se=F) + 
  labs( 
    y="PC1", 
    x="Latitude", 
    title = bquote(~ R^2 *'= 0.4991')) ## CHANGE ME ACCORDING TO R^2 VALUES summary(PC1lat)
## plot(gg2)
ggsave(filename = "RADseq_filt_PC1vlat.png", plot = gg2, device = "png", scale = 2, width = 8.5, height = 11, units = c("cm"), dpi = 600, limitsize = FALSE)

gg3 <- ggplot(gg.pca, aes(x=LONG, y=PC2)) + 
  geom_point() + 
  geom_smooth(method="lm", se=F) + 
  labs( 
    y="PC2", 
    x="Longitude", 
    title = bquote(~ R^2 *'= -0.005411')) ## CHANGE ME ACCORDING TO R^2 VALUES summary(PC2long)
## plot(gg3)
ggsave(filename = "RADseq_filt_PC2vlong.png", plot = gg3, device = "png", scale = 2, width = 8.5, height = 11, units = c("cm"), dpi = 600, limitsize = FALSE)

gg4 <- ggplot(gg.pca, aes(x=LAT, y=PC2)) + 
  geom_point() + 
  geom_smooth(method="lm", se=F) + 
  labs( 
    y="PC2", 
    x="Latitude", 
    title = bquote(~ R^2 *'= 0.09758')) ## CHANGE ME ACCORDING TO R^2 VALUES summary(PC2lat)
## plot(gg4)
ggsave(filename = "RADseq_filt_PC2vlat.png", plot = gg4, device = "png", scale = 2, width = 8.5, height = 11, units = c("cm"), dpi = 600, limitsize = FALSE)


## Cleaning up environment ------------------------------------
rm(list = c("AlleleFreqPlot", "col", "g1", "g2", "gg.pca", "gg1", "gg2", "gg3" , "gg4" , "mySum", "PC1", "PC1lat", "PC1long", "PC1vlat", "PC1vlong", "PC2", "PC2lat", "PC2long", "PC2vlat", "PC2vlong", "pca.1.loadings", "vcf", "x", "y"))
gc()


## PCA-based Map Stuff --------------------------------------------
## You can only have minimum 2 clusters or more
## For Cluster of 2
png(filename = "RADseq_filt_kof2.png")
grp <- find.clusters(aa.genlight, max.n.clust=12, glPca = pca.1, perc.pca = 100, n.iter=1e6, n.start=1000)
## will ask number of PCs to retain, hit enter to retain all
## will ask number of clusters, if using RStudio will show BIC but otherwise pick best options here
dev.off()
## export this data
write.table(grp$grp, file="RADseq_filt_kof2.txt", sep="\t", quote=F, col.names=F) 
## ugly quick hack to prepare the grouping of populations, not individuals 
x <- data.frame(keyName=names(grp$grp), value=grp$grp, row.names=NULL)  # convert vector into data frame 
x$pop <- as.factor(substr(x$keyName,5,9)) # get population identity 
y <- x[order(x$pop),] # order it 
grp.pop <- y[!duplicated(y$pop),] # remove duplicates (one line per pop) 
coords <- read.csv(file="20200528_RADseq_metadata.csv", header = T, na.strings = c("n/a"), sep = "\t")
## map <- get_stamenmap(location =  c(Long = -96, Lat = 60), zoom = 3) %>% register_google(key = "")
us <- c(left = -96, bottom = 40, right = -72, top = 57)
## 
register_google(key = "AIzaSyBAFbgj5zQZ2VOBroob_YbI59k875515h4", write = TRUE)
## map <- get_map(location = us, zoom = "auto", scale = "auto") 
## Toner lite maps
map <- get_stamenmap(us, zoom = 5, maptype = "toner-lite")
mapPoints <- ggmap(map) + 
  geom_point(aes(x = coords$Long, y = coords$Lat, colour = factor(grp.pop$value)), data = grp.pop) + 
  scale_colour_manual(values = c("red","blue","black")) 
pdf("RADseq_filt_kof2_StamenMap_tonerlite.pdf", height = 11, width = 8.5)
mapPoints 
dev.off()
## Terrain Line maps
map <- get_stamenmap(us, zoom = 5, maptype = "terrain-lines")
mapPoints <- ggmap(map) + 
  geom_point(aes(x = coords$Long, y = coords$Lat, colour = factor(grp.pop$value)), data = grp.pop) + 
  scale_colour_manual(values = c("red","blue","black")) 
pdf("RADseq_filt_kof2_StamenMap_terrainlines.pdf", height = 11, width = 8.5)
mapPoints 
dev.off()
## discriminant analysis of principal components (DAPC)
dapc1 <- dapc(aa.genlight, grp$grp, glPca = pca.1)
## Keep 150 of 300 PCs
## Select 1 - #k = 1 k to select from
col <- funky(5)
# Visualization of DAPC with Scatterplot and Barplot
pdf("RADseq_filt_kof2_DAPC_visual.pdf", height = 11, width = 8.5) 
scatter(dapc1)
compoplot(dapc1, cex.names = 0.4, legend=T, col=col)
dev.off()
## Exporting information
# Eigenvalues
dapc1.eig <- as.matrix(dapc1$eig)
write.table(dapc1.eig, file = "RADseq_filt_kof2_DAPC_eig.txt", sep = "\t")
# Prior group assignment
dapc1.grp <- as.matrix(dapc1$grp)
write.table(dapc1.grp, file = "RADseq_filt_kof2_DAPC_priorgroupassign.txt", sep = "\t")
# Prior group probabilities
dapc1.prior <- as.matrix(dapc1$prior)
write.table(dapc1.prior, file = "RADseq_filt_kof2_DAPC_priorgroupprob.txt", sep = "\t")
# Posterior group assignment
dapc1.assign <- as.matrix(dapc1$assign)
write.table(dapc1.assign, file = "RADseq_filt_kof2_DAPC_postgroupassign.txt", sep = "\t")
## Cleaning directory
rm(list = c("dapc1.grp", "dapc1.prior", "dapc1", "dapc1.assign", "dapc1.eig", "AlleleFreqPlot", "col", "g1", "g2", "gg.pca", "gg1", "gg2", "gg3" , "gg4" , "mySum", "PC1", "PC1lat", "PC1long", "PC1vlat", "PC1vlong", "PC2", "PC2lat", "PC2long", "PC2vlat", "PC2vlong", "pca.1.loadings", "vcf", "x", "y"))
gc()

## For Cluster of 3
png(filename = "RADseq_filt_kof3.png")
grp <- find.clusters(aa.genlight, max.n.clust=12, glPca = pca.1, perc.pca = 100, n.iter=1e6, n.start=1000)
## will ask number of PCs to retain, hit enter to retain all
## will ask number of clusters, if using RStudio will show BIC but otherwise pick best options here
dev.off()
## export this data
write.table(grp$grp, file="RADseq_filt_kof3.txt", sep="\t", quote=F, col.names=F) 
## ugly quick hack to prepare the grouping of populations, not individuals 
x <- data.frame(keyName=names(grp$grp), value=grp$grp, row.names=NULL)  # convert vector into data frame 
x$pop <- as.factor(substr(x$keyName,5,9)) # get population identity 
y <- x[order(x$pop),] # order it 
grp.pop <- y[!duplicated(y$pop),] # remove duplicates (one line per pop) 
coords <- read.csv(file="20200528_RADseq_metadata.csv", header = T, na.strings = c("n/a"), sep = "\t") 
## map <- get_stamenmap(location =  c(Long = -96, Lat = 60), zoom = 3) %>% register_google(key = "")
us <- c(left = -96, bottom = 40, right = -72, top = 57)
## 
## register_google(key = "AIzaSyBAFbgj5zQZ2VOBroob_YbI59k875515h4", write = TRUE)
## map <- get_map(location = us, zoom = "auto", scale = "auto") 
## Toner lite maps
map <- get_stamenmap(us, zoom = 5, maptype = "toner-lite")
mapPoints <- ggmap(map) + 
  geom_point(aes(x = coords$Long, y = coords$Lat, colour = factor(grp.pop$value)), data = grp.pop) + 
  scale_colour_manual(values = c("red","blue","black")) 
pdf("RADseq_filt_kof3_StamenMap_tonerlite.pdf", height = 11, width = 8.5)
mapPoints 
dev.off()
## Terrain Line maps
map <- get_stamenmap(us, zoom = 5, maptype = "terrain-lines")
mapPoints <- ggmap(map) + 
  geom_point(aes(x = coords$Long, y = coords$Lat, colour = factor(grp.pop$value)), data = grp.pop) + 
  scale_colour_manual(values = c("red","blue","black")) 
pdf("RADseq_filt_kof3_StamenMap_terrainlines.pdf", height = 11, width = 8.5)
mapPoints 
dev.off()
## discriminant analysis of principal components (DAPC)
dapc1 <- dapc(aa.genlight, grp$grp, glPca = pca.1)
## Keep 150 of 300 PCs
## Selecte 1 - #k = 2 k to select from
col <- funky(5)
# Visualization of DAPC with Scatterplot and Barplot
pdf("RADseq_filt_kof3_DAPC_visual.pdf", height = 11, width = 8.5) 
scatter(dapc1)
compoplot(dapc1, cex.names = 0.4, legend=T, col=col)
dev.off()
## Exporting information
# Eigenvalues
dapc1.eig <- as.matrix(dapc1$eig)
write.table(dapc1.eig, file = "RADseq_filt_kof3_DAPC_eig.txt", sep = "\t")
# Prior group assignment
dapc1.grp <- as.matrix(dapc1$grp)
write.table(dapc1.grp, file = "RADseq_filt_kof3_DAPC_priorgroupassign.txt", sep = "\t")
# Prior group probabilities
dapc1.prior <- as.matrix(dapc1$prior)
write.table(dapc1.prior, file = "RADseq_filt_kof3_DAPC_priorgroupprob.txt", sep = "\t")
# Posterior group assignment
dapc1.assign <- as.matrix(dapc1$assign)
write.table(dapc1.assign, file = "RADseq_filt_kof3_DAPC_postgroupassign.txt", sep = "\t")
## Cleaning directory
rm(list = c("dapc1.grp", "dapc1.prior", "dapc1", "dapc1.assign", "dapc1.eig", "AlleleFreqPlot", "col", "g1", "g2", "gg.pca", "gg1", "gg2", "gg3" , "gg4" , "mySum", "PC1", "PC1lat", "PC1long", "PC1vlat", "PC1vlong", "PC2", "PC2lat", "PC2long", "PC2vlat", "PC2vlong", "pca.1.loadings", "vcf", "x", "y"))
gc()

## For Cluster of 4
png(filename = "RADseq_filt_kof4.png")
grp <- find.clusters(aa.genlight, max.n.clust=12, glPca = pca.1, perc.pca = 100, n.iter=1e6, n.start=1000)
## will ask number of PCs to retain, hit enter to retain all
## will ask number of clusters, if using RStudio will show BIC but otherwise pick best options here
dev.off()
## export this data
write.table(grp$grp, file="RADseq_filt_kof4.txt", sep="\t", quote=F, col.names=F) 
## ugly quick hack to prepare the grouping of populations, not individuals 
x <- data.frame(keyName=names(grp$grp), value=grp$grp, row.names=NULL)  # convert vector into data frame 
x$pop <- as.factor(substr(x$keyName,5,9)) # get population identity 
y <- x[order(x$pop),] # order it 
grp.pop <- y[!duplicated(y$pop),] # remove duplicates (one line per pop) 
coords <- read.csv(file="20200528_RADseq_metadata.csv", header = T, na.strings = c("n/a"), sep = "\t") 
## map <- get_stamenmap(location =  c(Long = -96, Lat = 60), zoom = 3) %>% register_google(key = "")
us <- c(left = -96, bottom = 40, right = -72, top = 57)
## 
## register_google(key = "AIzaSyBAFbgj5zQZ2VOBroob_YbI59k875515h4", write = TRUE)
## map <- get_map(location = us, zoom = "auto", scale = "auto") 
## Toner lite maps
map <- get_stamenmap(us, zoom = 5, maptype = "toner-lite")
mapPoints <- ggmap(map) + 
  geom_point(aes(x = coords$Long, y = coords$Lat, colour = factor(grp.pop$value)), data = grp.pop) + 
  scale_colour_manual(values = c("red","blue","black","orange")) 
pdf("RADseq_filt_kof4_StamenMap_tonerlite.pdf", height = 11, width = 8.5)
mapPoints 
dev.off()
## Terrain Line maps
map <- get_stamenmap(us, zoom = 5, maptype = "terrain-lines")
mapPoints <- ggmap(map) + 
  geom_point(aes(x = coords$Long, y = coords$Lat, colour = factor(grp.pop$value)), data = grp.pop) + 
  scale_colour_manual(values = c("red","blue","black","orange")) 
pdf("RADseq_filt_kof4_StamenMap_terrainlines.pdf", height = 11, width = 8.5)
mapPoints 
dev.off()
## discriminant analysis of principal components (DAPC)
dapc1 <- dapc(aa.genlight, grp$grp, glPca = pca.1)
## Keep 150 of 300 PCs
## Selecte 1 - #k = 3 k to select from
col <- funky(5)
# Visualization of DAPC with Scatterplot and Barplot
pdf("RADseq_filt_kof4_DAPC_visual.pdf", height = 11, width = 8.5) 
scatter(dapc1)
compoplot(dapc1, cex.names = 0.4, legend=T, col=col)
dev.off()
## Exporting information
# Eigenvalues
dapc1.eig <- as.matrix(dapc1$eig)
write.table(dapc1.eig, file = "RADseq_filt_kof4_DAPC_eig.txt", sep = "\t")
# Prior group assignment
dapc1.grp <- as.matrix(dapc1$grp)
write.table(dapc1.grp, file = "RADseq_filt_kof4_DAPC_priorgroupassign.txt", sep = "\t")
# Prior group probabilities
dapc1.prior <- as.matrix(dapc1$prior)
write.table(dapc1.prior, file = "RADseq_filt_kof4_DAPC_priorgroupprob.txt", sep = "\t")
# Posterior group assignment
dapc1.assign <- as.matrix(dapc1$assign)
write.table(dapc1.assign, file = "RADseq_filt_kof4_DAPC_postgroupassign.txt", sep = "\t")
## Cleaning directory
rm(list = c("dapc1.grp", "dapc1.prior", "dapc1", "dapc1.assign", "dapc1.eig", "AlleleFreqPlot", "col", "g1", "g2", "gg.pca", "gg1", "gg2", "gg3" , "gg4" , "mySum", "PC1", "PC1lat", "PC1long", "PC1vlat", "PC1vlong", "PC2", "PC2lat", "PC2long", "PC2vlat", "PC2vlong", "pca.1.loadings", "vcf", "x", "y"))
gc()

## For Cluster of 5
png(filename = "RADseq_filt_kof5.png")
grp <- find.clusters(aa.genlight, max.n.clust=12, glPca = pca.1, perc.pca = 100, n.iter=1e6, n.start=1000)
## will ask number of PCs to retain, hit enter to retain all
## will ask number of clusters, if using RStudio will show BIC but otherwise pick best options here
dev.off()
## export this data
write.table(grp$grp, file="RADseq_filt_kof5.txt", sep="\t", quote=F, col.names=F) 
## ugly quick hack to prepare the grouping of populations, not individuals 
x <- data.frame(keyName=names(grp$grp), value=grp$grp, row.names=NULL)  # convert vector into data frame 
x$pop <- as.factor(substr(x$keyName,5,9)) # get population identity 
y <- x[order(x$pop),] # order it 
grp.pop <- y[!duplicated(y$pop),] # remove duplicates (one line per pop) 
coords <- read.csv(file="20200528_RADseq_metadata.csv", header = T, na.strings = c("n/a"), sep = "\t") 
## map <- get_stamenmap(location =  c(Long = -96, Lat = 60), zoom = 3) %>% register_google(key = "")
us <- c(left = -96, bottom = 40, right = -72, top = 57)
## 
## register_google(key = "AIzaSyBAFbgj5zQZ2VOBroob_YbI59k875515h4", write = TRUE)
## map <- get_map(location = us, zoom = "auto", scale = "auto") 
## Toner lite maps
map <- get_stamenmap(us, zoom = 5, maptype = "toner-lite")
mapPoints <- ggmap(map) + 
  geom_point(aes(x = coords$Long, y = coords$Lat, colour = factor(grp.pop$value)), data = grp.pop) + 
  scale_colour_manual(values = c("red","blue","black","orange", "green")) 
pdf("RADseq_filt_kof5_StamenMap_tonerlite.pdf", height = 11, width = 8.5)
mapPoints 
dev.off()
## Terrain Line maps
map <- get_stamenmap(us, zoom = 5, maptype = "terrain-lines")
mapPoints <- ggmap(map) + 
  geom_point(aes(x = coords$Long, y = coords$Lat, colour = factor(grp.pop$value)), data = grp.pop) + 
  scale_colour_manual(values = c("red","blue","black","orange", "green")) 
pdf("RADseq_filt_kof5_StamenMap_terrainlines.pdf", height = 11, width = 8.5)
mapPoints 
dev.off()
## discriminant analysis of principal components (DAPC)
dapc1 <- dapc(aa.genlight, grp$grp, glPca = pca.1)
## Keep 150 of 300 PCs
## Selecte 1 - #k = 4 k to select from
col <- funky(5)
# Visualization of DAPC with Scatterplot and Barplot
pdf("RADseq_filt_kof5_DAPC_visual.pdf", height = 11, width = 8.5) 
scatter(dapc1)
compoplot(dapc1, cex.names = 0.4, legend=T, col=col)
dev.off()
## Exporting information
# Eigenvalues
dapc1.eig <- as.matrix(dapc1$eig)
write.table(dapc1.eig, file = "RADseq_filt_kof5_DAPC_eig.txt", sep = "\t")
# Prior group assignment
dapc1.grp <- as.matrix(dapc1$grp)
write.table(dapc1.grp, file = "RADseq_filt_kof5_DAPC_priorgroupassign.txt", sep = "\t")
# Prior group probabilities
dapc1.prior <- as.matrix(dapc1$prior)
write.table(dapc1.prior, file = "RADseq_filt_kof5_DAPC_priorgroupprob.txt", sep = "\t")
# Posterior group assignment
dapc1.assign <- as.matrix(dapc1$assign)
write.table(dapc1.assign, file = "RADseq_filt_kof5_DAPC_postgroupassign.txt", sep = "\t")
## Cleaning directory
rm(list = c("dapc1.grp", "dapc1.prior", "dapc1", "dapc1.assign", "dapc1.eig", "AlleleFreqPlot", "col", "g1", "g2", "gg.pca", "gg1", "gg2", "gg3" , "gg4" , "mySum", "PC1", "PC1lat", "PC1long", "PC1vlat", "PC1vlong", "PC2", "PC2lat", "PC2long", "PC2vlat", "PC2vlong", "pca.1.loadings", "vcf", "x", "y"))
gc()

## For Cluster of 6
png(filename = "RADseq_filt_kof6.png")
grp <- find.clusters(aa.genlight, max.n.clust=12, glPca = pca.1, perc.pca = 100, n.iter=1e6, n.start=1000)
## will ask number of PCs to retain, hit enter to retain all
## will ask number of clusters, if using RStudio will show BIC but otherwise pick best options here
dev.off()
## export this data
write.table(grp$grp, file="RADseq_filt_kof6.txt", sep="\t", quote=F, col.names=F) 
## ugly quick hack to prepare the grouping of populations, not individuals 
x <- data.frame(keyName=names(grp$grp), value=grp$grp, row.names=NULL)  # convert vector into data frame 
x$pop <- as.factor(substr(x$keyName,5,9)) # get population identity 
y <- x[order(x$pop),] # order it 
grp.pop <- y[!duplicated(y$pop),] # remove duplicates (one line per pop) 
coords <- read.csv(file="20200528_RADseq_metadata.csv", header = T, na.strings = c("n/a"), sep = "\t") 
## map <- get_stamenmap(location =  c(Long = -96, Lat = 60), zoom = 3) %>% register_google(key = "")
us <- c(left = -96, bottom = 40, right = -72, top = 57)
## 
## register_google(key = "AIzaSyBAFbgj5zQZ2VOBroob_YbI59k875515h4", write = TRUE)
## map <- get_map(location = us, zoom = "auto", scale = "auto") 
## Toner lite maps
map <- get_stamenmap(us, zoom = 5, maptype = "toner-lite")
mapPoints <- ggmap(map) + 
  geom_point(aes(x = coords$Long, y = coords$Lat, colour = factor(grp.pop$value)), data = grp.pop) + 
  scale_colour_manual(values = c("red","blue","black","orange", "green", "purple")) 
pdf("RADseq_filt_kof6_StamenMap_tonerlite.pdf", height = 11, width = 8.5)
mapPoints 
dev.off()
## Terrain Line maps
map <- get_stamenmap(us, zoom = 5, maptype = "terrain-lines")
mapPoints <- ggmap(map) + 
  geom_point(aes(x = coords$Long, y = coords$Lat, colour = factor(grp.pop$value)), data = grp.pop) + 
  scale_colour_manual(values = c("red","blue","black","orange", "green", "purple")) 
pdf("RADseq_filt_kof6_StamenMap_terrainlines.pdf", height = 11, width = 8.5)
mapPoints 
dev.off()
## discriminant analysis of principal components (DAPC)
dapc1 <- dapc(aa.genlight, grp$grp, glPca = pca.1)
## Keep 150 of 300 PCs
## Selecte 1 - #k = 5 k to select from
col <- funky(5)
# Visualization of DAPC with Scatterplot and Barplot
pdf("RADseq_filt_kof6_DAPC_visual.pdf", height = 11, width = 8.5) 
scatter(dapc1)
compoplot(dapc1, cex.names = 0.4, legend=T, col=col)
dev.off()
## Exporting information
# Eigenvalues
dapc1.eig <- as.matrix(dapc1$eig)
write.table(dapc1.eig, file = "RADseq_filt_kof6_DAPC_eig.txt", sep = "\t")
# Prior group assignment
dapc1.grp <- as.matrix(dapc1$grp)
write.table(dapc1.grp, file = "RADseq_filt_kof6_DAPC_priorgroupassign.txt", sep = "\t")
# Prior group probabilities
dapc1.prior <- as.matrix(dapc1$prior)
write.table(dapc1.prior, file = "RADseq_filt_kof6_DAPC_priorgroupprob.txt", sep = "\t")
# Posterior group assignment
dapc1.assign <- as.matrix(dapc1$assign)
write.table(dapc1.assign, file = "RADseq_filt_kof6_DAPC_postgroupassign.txt", sep = "\t")
## Cleaning directory
rm(list = c("dapc1.grp", "dapc1.prior", "dapc1", "dapc1.assign", "dapc1.eig", "AlleleFreqPlot", "col", "g1", "g2", "gg.pca", "gg1", "gg2", "gg3" , "gg4" , "mySum", "PC1", "PC1lat", "PC1long", "PC1vlat", "PC1vlong", "PC2", "PC2lat", "PC2long", "PC2vlat", "PC2vlong", "pca.1.loadings", "vcf", "x", "y"))
gc()
  
## Calculation and visualization of Nei’s distances -------------
## Loading required packages 
library(vcfR)
library(adegenet)
library(adegraphics)
library(pegas)
library(StAMPP)
library(lattice)
library(gplots)
library(ape)
library(ggmap)

## Improrting files
vcf <- read.vcfR("pop3.r090.populations.snps.vcf.gz")

## Convert to genlight object
aa.genlight <- vcfR2genlight(vcf, n.cores=80)

## Add real SNP.names
locNames(aa.genlight) <- paste(vcf@fix[,1],vcf@fix[,2],sep="_")

## Add population names, here the group names are the first 3 characters of the ind name
pop(aa.genlight) <- substr(indNames(aa.genlight),1,3)

## Cleaning up directory
rm(list = c("vcf"))
gc(reset = TRUE)

### Calculate pairwise Fst among populations 
aa.genlight@ploidy <- as.integer(ploidy(aa.genlight))
# nclusters is the number of proccesor treads or cores to use during calculations
# nboots is number of bootstraps to perform across loci to generate confidence intervals and p-values
# percent is the percentile to calculate the confidence interval around
aa.fst <- stamppFst(aa.genlight, nboots=10, percent=99, nclusters=6)
## Modify the matrix for opening in SplitsTree
aa.fst.sym <- aa.fst
## Add upper triangle
aa.fst.sym[upper.tri(aa.fst.sym)] <- t(aa.fst.sym)[upper.tri(aa.fst.sym)]
## Replace NAs with zero
aa.fst.sym[is.na(aa.fst.sym)] <- 0 
## Export matrix - for SplitsTree
stamppPhylip(aa.fst.sym, file="RADseq_filt_pairFst_allphy.dst") 
## Now can open the *dstfiles in SplitsTree

## Calculate Nei's 1972 distance between pops 
aa.D.pop <- stamppNeisD(aa.genlight, pop = TRUE)
## Export matrix - for SplitsTree 
stamppPhylip(aa.D.pop, file="RADseq_filt_Neisdist_popphy.dst")
### heatmap of the pops distance matrix 
colnames(aa.D.pop) <- rownames(aa.D.pop)    
pdf(file="RADseq_filt_Neisdist_popheatmap.pdf", height = 11, width = 8.5) 
heatmap.2(aa.D.pop, trace="none", cexRow=0.4, cexCol=0.4)
dev.off()
## plot and save NJ tree
pdf(file="RADseq_filt_Neisdist_popnjtree.pdf", height = 11, width = 8.5)
plot(nj(aa.D.pop)) 
dev.off()
write.tree(nj(aa.D.pop), file="RADseq_filt_NJNeisdist_poptree.tre")

### Calculate Nei's 1972 distances between individuals
aa.D.ind <- stamppNeisD(aa.genlight, pop = FALSE)
# Export matrix for opening in SplitsTree
stamppPhylip(aa.D.ind, file="RADseq_filt_Neisdist_indphy.dst")
### heatmap of the pops distance matrix 
colnames(aa.D.ind) <- rownames(aa.D.ind)    
pdf(file="RADseq_filt_Neisdist_indheatmap.pdf", height = 22, width = 17) 
heatmap.2(aa.D.ind, trace="none", cexRow=0.4, cexCol=0.4)
dev.off()
## plot and save NJ tree 
pdf(file="RADseq_filt_Neisdist_ind_njtree.pdf", height = 22, width = 17)
plot(nj(aa.D.ind)) 
dev.off()
write.tree(nj(aa.D.ind), file="RADseq_filt_NJNeisdist_indtree.tre")




